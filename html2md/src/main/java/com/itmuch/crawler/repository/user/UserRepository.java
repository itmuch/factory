package com.itmuch.crawler.repository.user;

import com.itmuch.crawler.domain.user.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends ElasticsearchRepository<User, Long> {
    User findByGitHubId(Integer gitHubId);

    User findByUsername(String usernameFromToken);
}
