package com.itmuch.crawler.repository;

import com.itmuch.crawler.domain.content.Tag;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends ElasticsearchRepository<Tag, String> {
}
