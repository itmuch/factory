package com.itmuch.crawler.repository;

import com.itmuch.crawler.domain.content.Site;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotBlank;

@Repository
public interface SiteRepository extends ElasticsearchRepository<Site, String> {

    Page<Site> findByName(String name, Pageable pageable);

    Site findByUrl(@NotBlank String url);
}
