package com.itmuch.crawler.repository;

import com.itmuch.crawler.domain.job.JobConfiguration;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface JobConfigurationRepository extends ElasticsearchRepository<JobConfiguration, String> {
}
