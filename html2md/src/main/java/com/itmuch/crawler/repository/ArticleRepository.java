package com.itmuch.crawler.repository;

import com.itmuch.crawler.domain.AuditEnum;
import com.itmuch.crawler.domain.content.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

@SuppressWarnings("ALL")
@Repository
public interface ArticleRepository extends ElasticsearchRepository<Article, Long> {

    Page<Article> findByAuditAndGatherDateGreaterThan(AuditEnum audit, long gatherDate, Pageable pageable);

    Page<Article> findByAuditAndGatherDateLessThan(AuditEnum audit, long gatherDate, Pageable pageable);


    Article findByHash(String hash);

    Page<Article> findByTagsIsIn(List<String> tags, Pageable pageable);

    Page<Article> findBySiteId(String siteId, Pageable pageable);

    Page<Article> findByAudit(AuditEnum auditEnum, Pageable pageable);

    Stream<Article> findAllByIdIn(Long[] id);
}
