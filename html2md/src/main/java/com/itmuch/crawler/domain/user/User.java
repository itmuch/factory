package com.itmuch.crawler.domain.user;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.itmuch.crawler.core.convert.LongToStringJsonSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "user_index", type = "user_type")
public class User {
    /**
     * ID
     */
    @Id
    @JsonSerialize(using = LongToStringJsonSerializer.class)
    private Long id;

    /**
     * GitHub id
     */
    private Integer gitHubId;

    /**
     * 用户角色
     */
    private String role;

    /**
     * 用户名
     */
    private String username;

    /**
     * 个人主页
     */
    private String homepage;

    /**
     * 头像地址
     */
    private String avatar;

    /**
     * 个性签名
     */
    private String bio;
}