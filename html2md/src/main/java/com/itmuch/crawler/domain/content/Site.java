package com.itmuch.crawler.domain.content;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.URL;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Document(indexName = "site_index", type = "site_type")
@Data
@Builder
@ToString
public class Site {
    @Id
    private String id;

    @NotBlank
    private String name;

    @URL
    private String url;

    /**
     * 站点类型
     */
    private Integer type;

    /**
     * 站点图标地址
     */
    private String icon;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;
}
