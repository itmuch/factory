package com.itmuch.crawler.domain.content;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "tag_index", type = "tag_type")
@Data
@Builder
@ToString
public class Tag {
    /**
     * 标签名称，这里将标签名称设为id
     * 这样做的目的：方便在article中存储（如果额外弄个标签id还得再关联查询）
     */
    @Id
    private String name;

    /**
     * 图标地址
     */
    private String iconUrl;
}