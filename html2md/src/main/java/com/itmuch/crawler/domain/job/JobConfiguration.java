package com.itmuch.crawler.domain.job;


import com.itmuch.crawler.domain.content.Site;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Document(indexName = "job_configuration_index", type = "job_configuration_type")
public class JobConfiguration {

    public static final String JOB_DEFAULT_GROUP = "JOB_DEFAULT_GROUP";
    /**
     * job的id，唯一标识。
     */
    @Id
    private String id;

    @Transient
    @Valid
    private Site site;

    /**
     * 任务名称，
     */
    @NotBlank
    private String name;

//    /**
//     * 任务组id
//     */
//    @NonNull
//    private String jobGroupId;
    /**
     * 任务分组
     */
    private String group;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 任务状态
     */
    private String status;

    /**
     * cron表达式
     */
    private String cronExpression;

    /**
     * 简单时间表达式
     */
    private Date simpleExpression;

    /**
     * 任务执行时调用哪个类的方法 包名+类名
     */
    private String jobClassName;

    /**
     * 任务调用的方法名
     */
    private String jobMethodName;

    /**
     * job的参数
     */
    private String jobParam;

    /**
     * 任务是否有状态(即并发执行同一个任务)
     */
    @Builder.Default
    private boolean concurrent = true;

    /**
     * 描述
     */
    private String description;

}