package com.itmuch.crawler.util.snowflake;

/**
 * Time service.
 *
 * @author zhangliang
 */
public class TimeService {

    /**
     * Get current millis.
     *
     * @return current millis
     */
    public long getCurrentMillis() {
        return System.currentTimeMillis();
    }
}