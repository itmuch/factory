package com.itmuch.crawler.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.util.Properties;

@Configuration
public class QuartzConfiguration {
    /**
     * 实例化quartz调度工厂
     *
     * @return scheduler
     */
    @Bean(name = "scheduler")
    public SchedulerFactoryBean schedulerFactory() {
        SchedulerFactoryBean bean = new SchedulerFactoryBean();
        // 用于quartz集群,QuartzScheduler 启动时更新己存在的Job
        bean.setOverwriteExistingJobs(true);
        bean.setQuartzProperties(
                this.quartzProperties()
        );
        // 延时启动，应用启动0秒后
        bean.setStartupDelay(0);
        // 注册触发器
//        bean.setTriggers(jobTrigger);
        return bean;
    }

    /**
     * 设置quartz属性
     *
     * @return 属性
     */
    private Properties quartzProperties() {
        Properties prop = new Properties();
        // 设置instanceId，否则启动日志会有个NON_CLUSTERED
        // 参考文档：http://lihao312.iteye.com/blog/2329374
        prop.put("org.quartz.scheduler.instanceId", "ITMUCH");
        prop.put("org.quartz.jobStore.class", "net.joelinn.quartz.jobstore.RedisJobStore");
        prop.put("org.quartz.jobStore.host", "localhost");
        prop.put("org.quartz.jobStore.misfireThreshold", "60000");
        prop.put("org.quartz.jobStore.port", "6379");

        return prop;
    }
}