package com.itmuch.crawler.config;

import com.roncoo.shiro.freemarker.ShiroTags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class FreemarkerConfiguration {
    private final freemarker.template.Configuration configuration;

    @Autowired
    public FreemarkerConfiguration(freemarker.template.Configuration configuration) {
        this.configuration = configuration;
    }

    @PostConstruct
    public void setSharedVariable() {
        configuration.setSharedVariable("shiro", new ShiroTags());
    }
}
