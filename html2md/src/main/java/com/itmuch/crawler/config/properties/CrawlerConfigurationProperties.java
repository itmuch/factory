package com.itmuch.crawler.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "itmuch")
@Data
@Configuration
public class CrawlerConfigurationProperties {
    @NestedConfigurationProperty
    private Token token;
}
