package com.itmuch.crawler.config;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.mgt.RememberMeManager;
import org.apache.shiro.spring.config.web.autoconfigure.ShiroWebAutoConfiguration;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShiroWebConfiguration extends ShiroWebAutoConfiguration {
    @Bean
    @Override
    protected RememberMeManager rememberMeManager() {
        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
        cookieRememberMeManager.setCookie(rememberMeCookieTemplate());
        // 必须加上这一行，否则这个CipherKey的初始化是在：org.apache.shiro.mgt.AbstractRememberMeManager()中执行的。
        // 每次启动生成的CipherKey都不同，于是会导致应用重启后，无法解密先前Remember Me的用户信息。
        cookieRememberMeManager.setCipherKey(
                Base64.decode("4AvVhmFLUs0XaA3Kprsdag==")
        );
        return cookieRememberMeManager;
    }

    @Override
    @Bean
    public ShiroFilterChainDefinition shiroFilterChainDefinition() {
        DefaultShiroFilterChainDefinition chainDefinition = new DefaultShiroFilterChainDefinition();
        chainDefinition.addPathDefinition("/logout", "logout");
        chainDefinition.addPathDefinition("/login", "authc");
        chainDefinition.addPathDefinition("/**", "anon");
        chainDefinition.addPathDefinition("/admin/**", "roles['ADMIN']");

        //        chainDefinition.addPathDefinition("/login/github**", "anon");
        //        chainDefinition.addPathDefinition("/login/github/callback", "anon");
        //        chainDefinition.addPathDefinition("/", "anon");
        return chainDefinition;
    }
}
