package com.itmuch.crawler.config.properties;

import lombok.Data;

@Data
public class Token {
    private String secret;
    private long expiration;
}