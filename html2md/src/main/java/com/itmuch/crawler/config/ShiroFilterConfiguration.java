package com.itmuch.crawler.config;

import com.google.common.collect.Maps;
import com.itmuch.crawler.repository.user.UserRepository;
import com.itmuch.crawler.security.jwt.TokenUtils;
import com.itmuch.crawler.security.shiro.ExtendAnonymousFilter;
import com.itmuch.crawler.security.shiro.ExtendFormAuthenticationFilter;
import com.itmuch.crawler.security.shiro.ExtendUserFilter;
import com.itmuch.crawler.security.shiro.JwtRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.config.web.autoconfigure.ShiroWebFilterConfiguration;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.HashMap;

@Configuration
public class ShiroFilterConfiguration extends ShiroWebFilterConfiguration {
    @Bean(name = "realm")
    public Realm realm(TokenUtils tokenUtils, UserRepository userRepository) {
        return new JwtRealm(tokenUtils, userRepository);
    }

    /**
     * Order问题：自定义的Filter必须在创建后实例化，否则将会导致过滤器工作不正常。
     * 我遇到的现象是：例如我重写了authc这个过滤器，然后配置：
     * /login=login
     * /login/github=anon
     * /**=authc
     * 当请求/login/github时，却也会走自定义的authc过滤器。即：导致所有的url请求，都会被authc这个拦截器拦截。
     * <p>
     * http://shiro-user.582556.n2.nabble.com/Shiro-integration-with-Spring-Boot-and-filter-chain-ordering-td7580288.html
     * http://richardadams606blog.blogspot.ca/2014/10/apache-shiro-and-spring-boot.html
     *
     * @return filters
     */
    @Bean
    protected ShiroFilterFactoryBean shiroFilterFactoryBean(UserRepository userRepository) {
        ShiroFilterFactoryBean filterFactoryBean = super.shiroFilterFactoryBean();
        HashMap<String, Filter> map = Maps.newHashMap();
        map.put("authc", new ExtendFormAuthenticationFilter(userRepository));
        map.put("user", new ExtendUserFilter(userRepository));
        map.put("anon", new ExtendAnonymousFilter(userRepository));
        filterFactoryBean.setFilters(map);
        return filterFactoryBean;
    }
}
