package com.itmuch.crawler.quartz.factory;

import com.itmuch.crawler.domain.job.JobConfiguration;
import com.itmuch.crawler.quartz.manager.JobConstants;
import com.itmuch.crawler.util.mapper.JsonMapper;
import org.quartz.*;

/**
 * 不允许concurrent
 */
@DisallowConcurrentExecution
public class DisallowConcurrentJobFactory extends AbstractJobFactory implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap mergedJobDataMap = context.getMergedJobDataMap();

        Object o = mergedJobDataMap.get(JobConstants.SCHEDULER_NAME);

        JobConfiguration configuration = JsonMapper.defaultMapper()
                .fromJson(o.toString(), JobConfiguration.class);

        if (configuration == null) {
            throw new JobExecutionException("该Job的配置不存在");
        }

        invoke(configuration);
    }
}