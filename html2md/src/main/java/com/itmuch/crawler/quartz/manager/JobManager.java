package com.itmuch.crawler.quartz.manager;

import com.itmuch.crawler.domain.job.JobConfiguration;
import com.itmuch.crawler.quartz.factory.DefaultJobFactory;
import com.itmuch.crawler.quartz.factory.DisallowConcurrentJobFactory;
import com.itmuch.crawler.util.mapper.JsonMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 任务调度管理器， 实现任务的动态操作
 */
@Component
@Slf4j
public class JobManager {
    private final Scheduler scheduler;

    @Autowired
    public JobManager(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    /**
     * 添加任务
     *
     * @param configuration 配置
     * @throws SchedulerException 异常
     */
    public void dealJob(JobConfiguration configuration) throws SchedulerException {
        // 入参为空，或者没有jobId直接return
        //        if (configuration == null || StringUtils.isBlank(configuration.getId())) {
        //            return;
        //        }

        // 不存在cron表达式，也不存在simple表达式，直接return，不处理该任务。
        if (StringUtils.isBlank(configuration.getCronExpression())
                && null == configuration.getSimpleExpression()) {
            log.info("cron/simple表达式均不存在，故而丢弃任务，configuration = {}", configuration);
            return;
        }

        // 未配置job的className，直接return
        // TODO 该选项目前强制填写，后面考虑支持springId
        if (StringUtils.isBlank(configuration.getJobClassName())) {
            return;
        }

        // 如果没有设置simple表达式，则认为是cron configuration
        if (null == configuration.getSimpleExpression()) {
            addCronJob(configuration);
        }
        // 认为是simple configuration
        else {
            addSimpleJob(configuration);
        }
    }

    /**
     * 添加 cron job
     *
     * @param configuration configuration
     * @throws SchedulerException 异常
     */
    private void addCronJob(JobConfiguration configuration) throws SchedulerException {
        //根据任务id和任务组Id创建触发器key
        String jobId = configuration.getId();
        String jobGroup = configuration.getGroup();

        TriggerKey triggerKey = TriggerKey.triggerKey(jobId, jobGroup);
        //获取触发器对象
        CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);

        String cronExpression = configuration.getCronExpression();
        // 如果不存在，创建一个
        if (null == trigger) {
            JobDetail jobDetail = JobBuilder
                    .newJob(
                            configuration.isConcurrent() ? DefaultJobFactory.class : DisallowConcurrentJobFactory.class
                    )
                    .withIdentity(
                            jobId,
                            jobGroup
                    )
                    .build();

            jobDetail.getJobDataMap()
                    .put(
                            JobConstants.SCHEDULER_NAME,
                            JsonMapper.defaultMapper().toJson(configuration)
                    );

            trigger = TriggerBuilder.newTrigger()
                    .withIdentity(triggerKey)
                    .withSchedule(
                            CronScheduleBuilder.cronSchedule(cronExpression)
                    )
                    .build();
            // String status = configuration.getStatus();
            scheduler.scheduleJob(jobDetail, trigger);
        }
        // 如果该Job已存在
        else {
            updateJobCron(configuration);
        }
    }

    /**
     * 添加simple job
     *
     * @param configuration 配置
     * @throws SchedulerException 异常
     */
    private void addSimpleJob(JobConfiguration configuration) throws SchedulerException {
        //根据任务id和任务组Id创建触发器key
        String jobId = configuration.getId();
        String jobGroup = configuration.getGroup();

        TriggerKey triggerKey = TriggerKey.triggerKey(jobId, jobGroup);
        //获取触发器对象
        SimpleTrigger trigger = (SimpleTrigger) scheduler.getTrigger(triggerKey);
        // 不存在，创建一个
        if (null == trigger) {
            JobDetail jobDetail = JobBuilder
                    .newJob(configuration.isConcurrent() ? DefaultJobFactory.class : DisallowConcurrentJobFactory.class)
                    .withIdentity(
                            jobId,
                            jobGroup
                    ).build();

            jobDetail.getJobDataMap()
                    .put(
                            JobConstants.SCHEDULER_NAME,
                            JsonMapper.defaultMapper().toJson(configuration)
                    );

            trigger = TriggerBuilder.newTrigger()
                    .withIdentity(triggerKey)
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule())
                    .startAt(configuration.getSimpleExpression()).build();
            scheduler.scheduleJob(jobDetail, trigger);
        } else {
            updateJobSimple(configuration);
        }
    }

    /**
     * 更新job cron表达式
     *
     * @param configuration configuration
     * @throws SchedulerException 异常
     */
    private void updateJobCron(JobConfiguration configuration) throws SchedulerException {
        String cronExpression = configuration.getCronExpression();

        String jobId = configuration.getId();
        String jobGroup = configuration.getGroup();
        TriggerKey triggerKey = TriggerKey.triggerKey(jobId, jobGroup);
        CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);

        // 如果Cron表达式没有发生变化，直接return
        if (cronExpression.equals(trigger.getCronExpression())) {
            return;
        }

        trigger = trigger.getTriggerBuilder()
                .withIdentity(triggerKey)
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronExpression)
                ).build();
        scheduler.rescheduleJob(triggerKey, trigger);
    }

    /**
     * 更新job simple表达式
     *
     * @param configuration configuration
     * @throws SchedulerException 异常
     */
    private void updateJobSimple(JobConfiguration configuration) throws SchedulerException {
        Date simpleExpression = configuration.getSimpleExpression();

        String jobId = configuration.getId();
        String jobGroup = configuration.getGroup();
        TriggerKey triggerKey = TriggerKey.triggerKey(jobId, jobGroup);
        SimpleTrigger trigger = (SimpleTrigger) scheduler.getTrigger(triggerKey);

        // 如果simple表达式没有发生变化，直接return
        if (simpleExpression.equals(trigger.getStartTime())) {
            return;
        }

        trigger = trigger.getTriggerBuilder()
                .withIdentity(triggerKey)
                .withSchedule(
                        SimpleScheduleBuilder.simpleSchedule()
                )
                .startAt(simpleExpression)
                .build();
        scheduler.rescheduleJob(triggerKey, trigger);
    }

    public String queryStatus(JobConfiguration configuration) throws SchedulerException {
        String jobId = configuration.getId();
        String jobGroup = configuration.getGroup();
        TriggerKey triggerKey = TriggerKey.triggerKey(jobId, jobGroup);

        Trigger.TriggerState triggerState = scheduler.getTriggerState(triggerKey);
        return triggerState.name();
    }


    /**
     * 获取所有job列表
     *
     * @return job列表
     * @throws SchedulerException 异常
     */
    @SuppressWarnings("Duplicates")
    public List<JobConfiguration> queryAllJob() throws SchedulerException {
        GroupMatcher<JobKey> matcher = GroupMatcher.anyJobGroup();

        Set<JobKey> jobKeys = scheduler.getJobKeys(matcher);

        List<JobConfiguration> jobList = new ArrayList<>();
        for (JobKey jobKey : jobKeys) {
            List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
            for (Trigger trigger : triggers) {
                JobConfiguration job = new JobConfiguration();
                job.setId(jobKey.getName());
                job.setGroup(jobKey.getGroup());

                Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
                job.setStatus(triggerState.name());

                if (trigger instanceof CronTrigger) {
                    CronTrigger cronTrigger = (CronTrigger) trigger;
                    String cronExpression = cronTrigger.getCronExpression();
                    job.setCronExpression(cronExpression);
                } else if (trigger instanceof SimpleTrigger) {
                    SimpleTrigger simpleTrigger = (SimpleTrigger) trigger;
                    Date startTime = simpleTrigger.getStartTime();
                    job.setSimpleExpression(startTime);
                }
                jobList.add(job);
            }
        }
        return jobList;
    }

    /**
     * 获取所有正在运行的job
     *
     * @return 正在运行的job
     * @throws SchedulerException 异常
     */
    @SuppressWarnings({"unused", "Duplicates"})
    protected List<JobConfiguration> queryRunningJob() throws SchedulerException {
        List<JobExecutionContext> executingJobs = scheduler.getCurrentlyExecutingJobs();

        List<JobConfiguration> jobList = new ArrayList<>(executingJobs.size());

        for (JobExecutionContext executingJob : executingJobs) {
            Trigger trigger = executingJob.getTrigger();
            JobDetail jobDetail = executingJob.getJobDetail();
            JobKey jobKey = jobDetail.getKey();
            Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());

            JobConfiguration job = new JobConfiguration();
            job.setId(jobKey.getName());
            job.setGroup(jobKey.getGroup());
            job.setStatus(triggerState.name());
            if (trigger instanceof CronTrigger) {
                CronTrigger cronTrigger = (CronTrigger) trigger;
                String cronExpression = cronTrigger.getCronExpression();
                job.setCronExpression(cronExpression);
            } else if (trigger instanceof SimpleTrigger) {
                SimpleTrigger simpleTrigger = (SimpleTrigger) trigger;
                Date startTime = simpleTrigger.getStartTime();
                job.setSimpleExpression(startTime);
            }

            jobList.add(job);
        }
        return jobList;
    }

    /**
     * 暂停job
     *
     * @param configuration job的配置
     * @throws SchedulerException 异常
     */
    public void pauseJob(JobConfiguration configuration) throws SchedulerException {
        JobKey jobKey = JobKey.jobKey(
                configuration.getId(),
                configuration.getGroup()
        );
        scheduler.pauseJob(jobKey);
    }

    /**
     * 恢复job
     *
     * @param configuration job的配置
     * @throws SchedulerException 异常
     */
    public void resumeJob(JobConfiguration configuration) throws SchedulerException {
        JobKey jobKey = JobKey.jobKey(
                configuration.getId(),
                configuration.getGroup()
        );
        scheduler.resumeJob(jobKey);
    }

    /**
     * 删除job
     *
     * @param configuration 配置
     * @throws SchedulerException 异常
     */
    public void deleteJob(JobConfiguration configuration) throws SchedulerException {
        JobKey jobKey = JobKey.jobKey(
                configuration.getId(),
                configuration.getGroup()
        );
        scheduler.deleteJob(jobKey);
    }

    /**
     * 立即执行job
     *
     * @param configuration 配置
     * @throws SchedulerException 异常
     */
    @SuppressWarnings("unused")
    public void triggerJob(JobConfiguration configuration) throws SchedulerException {
        JobKey jobKey = JobKey.jobKey(
                configuration.getId(),
                configuration.getGroup()
        );
        scheduler.triggerJob(jobKey);
    }
}