package com.itmuch.crawler.quartz.job;

import com.itmuch.crawler.domain.content.Site;
import com.itmuch.crawler.repository.SiteRepository;
import com.itmuch.crawler.util.mapper.JsonMapper;
import com.itmuch.crawler.webmagic.config.BlogCrawlerConfiguration;
import com.itmuch.crawler.webmagic.pipeline.ElasticsearchPipeline;
import com.itmuch.crawler.webmagic.processor.BlogPageProcessor;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;

import java.io.IOException;

/**
 * 博客爬虫Job
 */
@Component
@Slf4j
public class BlogCrawlerJob {
    private final ElasticsearchPipeline elasticsearchPipeline;
    private final SiteRepository siteRepository;

    @Autowired
    public BlogCrawlerJob(ElasticsearchPipeline elasticsearchPipeline, SiteRepository siteRepository) {
        this.elasticsearchPipeline = elasticsearchPipeline;
        this.siteRepository = siteRepository;
    }

    @SuppressWarnings("unused")
    public void execute(String param) throws IOException {
        BlogCrawlerConfiguration blogCrawlerConfiguration = JsonMapper
                .defaultMapper()
                .fromJson(param, BlogCrawlerConfiguration.class);

        String siteUrl = blogCrawlerConfiguration.getSiteUrl();
        Site site = this.siteRepository.findByUrl(
                QueryParser.escape(siteUrl)
        );
        if (site == null) {
            log.error("定时任务无法执行，siteUrl = {}， site为空", siteUrl);
            return;
        }

        // 这一行非常重要，用来设置Article对象中的siteId.
        blogCrawlerConfiguration.setSiteId(site.getId());
        Spider.create(new BlogPageProcessor(blogCrawlerConfiguration))
                // 采集目标地址
                .addUrl(siteUrl)
                // 存储
                .addPipeline(this.elasticsearchPipeline)
                // 设置线程数
                .thread(10)
                //启动爬虫
                .run();

    }
}