package com.itmuch.crawler.quartz.manager;

public final class JobConstants {

    /**
     * 调度名称
     */
    public static final String SCHEDULER_NAME = "scheduler";
}