package com.itmuch.crawler.quartz;

import com.google.common.collect.Lists;
import com.itmuch.crawler.domain.job.JobConfiguration;
import com.itmuch.crawler.quartz.manager.JobManager;
import com.itmuch.crawler.repository.JobConfigurationRepository;
import lombok.extern.slf4j.Slf4j;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

/**
 * 定时任务参考：
 * http://www.cnblogs.com/darkwind/p/5447324.html
 * http://snailxr.iteye.com/blog/2076903
 * http://lihao312.iteye.com/blog/2329374
 */
@Configuration
@Slf4j
public class ScheduleRefreshDatabase {
    private final JobConfigurationRepository cronJobConfigurationRepository;
    private final JobManager jobManager;

    @Autowired
    public ScheduleRefreshDatabase(JobConfigurationRepository cronJobConfigurationRepository, JobManager jobManager) {
        this.cronJobConfigurationRepository = cronJobConfigurationRepository;
        this.jobManager = jobManager;
    }

    /**
     * 每隔5s查库，并根据查询结果决定是否重新设置定时任务
     *
     * @throws SchedulerException 异常
     */
    @Scheduled(fixedRate = 5000)
    public void syncJobsWithDB() throws SchedulerException {
        // 处理cron job
        Iterable<JobConfiguration> allInDB = cronJobConfigurationRepository.findAll();
        List<String> jobIdsInDB = Lists.newArrayList();
        allInDB.forEach(t -> jobIdsInDB.add(t.getId()));

        // schedule中所有的job，注意
        List<JobConfiguration> allJob = this.jobManager.queryAllJob();

        // 第一步：删除quartz中多余的任务
        allJob.forEach(t -> {
            // 如果quartz里面有数据库中不存在的元素，则删除该job
            if (!jobIdsInDB.contains(t.getId())) {
                try {
                    this.jobManager.deleteJob(t);
                    log.info("删除quartz中多余的Job, jobId = {}", t.getId());
                } catch (SchedulerException e) {
                    log.error("删除任务时发生异常", e);
                }
            }
        });

        // 第二步：处理任务
        allInDB.forEach(item -> {
            try {
                // 添加Job或者修改表达式
                this.jobManager.dealJob(item);

                // 将Job的任务状态同步到数据库。
                String status = this.jobManager.queryStatus(item);
                log.debug("Job：{}在quartz中的状态是{}", item.getName(), status);
                item.setStatus(status);
                this.cronJobConfigurationRepository.save(item);
            } catch (SchedulerException e) {
                log.error("处理任务时发生异常", e);
            }
        });
    }
}