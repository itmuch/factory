package com.itmuch.crawler.quartz.factory;

import com.itmuch.crawler.domain.job.JobConfiguration;
import com.itmuch.crawler.quartz.manager.JobConstants;
import com.itmuch.crawler.util.mapper.JsonMapper;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 默认job factory，允许concurrent
 */
public class DefaultJobFactory extends AbstractJobFactory implements Job {

    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap mergedJobDataMap = context.getMergedJobDataMap();
        Object jsonObject = mergedJobDataMap.get(JobConstants.SCHEDULER_NAME);

        JobConfiguration configuration = JsonMapper.defaultMapper()
                .fromJson(jsonObject.toString(), JobConfiguration.class);

        if (configuration == null) {
            throw new JobExecutionException("该Job的配置不存在。");
        }
        invoke(configuration);
    }
}