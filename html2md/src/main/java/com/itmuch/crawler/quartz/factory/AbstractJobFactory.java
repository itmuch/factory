package com.itmuch.crawler.quartz.factory;

import com.itmuch.crawler.domain.job.JobConfiguration;
import com.itmuch.crawler.util.ApplicationContextProvider;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * job工厂抽象类
 * 作用：使用反射，调用指定对象的指定方法。
 */
@Slf4j
public class AbstractJobFactory {
    public void invoke(JobConfiguration job) {
        try {
            log.info("开始执行任务，name = {}", job.getName());
            Class<?> clazz = Class.forName(job.getJobClassName());
            //从spring容器获取bean,否则无法注入
            Object obj = ApplicationContextProvider.getBean(clazz);
            //反射方法
            String jobMethodName = job.getJobMethodName();
            String jobParam = job.getJobParam();
            Method method = obj
                    .getClass()
                    .getDeclaredMethod(jobMethodName, String.class);

            // 调用方法
            method.invoke(obj, jobParam);
            log.debug("Job执行完成，name = {}", job.getName());
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            log.error("调度时发生异常。", e);
        }
    }
}