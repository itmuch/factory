package com.itmuch.crawler.controller.front.content;

import com.itmuch.crawler.core.constants.ConstantsApp;
import com.itmuch.crawler.domain.content.Article;
import com.itmuch.crawler.domain.content.Site;
import com.itmuch.crawler.repository.ArticleRepository;
import com.itmuch.crawler.repository.SiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
@RequestMapping("/sites")
public class SiteController {
    private final SiteRepository siteRepository;
    private final ArticleRepository articleRepository;

    @Autowired
    public SiteController(SiteRepository siteRepository, ArticleRepository articleRepository) {
        this.siteRepository = siteRepository;
        this.articleRepository = articleRepository;
    }

    @GetMapping("/list")
    public String list(
            Model model,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) Integer pageNo
    ) {
        if (pageNo == null) {
            pageNo = 1;
        }

        Page<Site> page = this.siteRepository.findByName(name,
                PageRequest.of(pageNo - 1, 10)
        );
        model.addAttribute("page", page);
        return "front/sites/list";
    }


    @GetMapping("/{siteId}/{pageNo}")
    public String lis(Model model, @PathVariable String siteId, @PathVariable Integer pageNo) {
        Optional<Site> siteInDB = this.siteRepository.findById(siteId);
        // id对应的站点不存在，直接404
        if (!siteInDB.isPresent()) {
            return ConstantsApp.PAGE_NOT_FOUND;
        }

        Page<Article> page = this.articleRepository.findBySiteId(
                siteId,
                PageRequest.of(pageNo - 1, 10, Sort.Direction.DESC, "gatherDate")
        );

        model.addAttribute("page", page);
        return "list";
    }
}
