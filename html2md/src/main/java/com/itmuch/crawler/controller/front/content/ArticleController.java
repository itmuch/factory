package com.itmuch.crawler.controller.front.content;

import com.itmuch.crawler.core.constants.ConstantsApp;
import com.itmuch.crawler.domain.AuditEnum;
import com.itmuch.crawler.domain.content.Article;
import com.itmuch.crawler.repository.ArticleRepository;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Controller
public class ArticleController {
    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleController(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    /**
     * 首页
     *
     * @param model model
     * @return 文章列表
     */
    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("title", "首页");
        return this.queryArticleList(model, 1);
    }

    /**
     * 列表页
     *
     * @param model  model
     * @param pageNo 文章列表
     * @return 列表
     */
    @GetMapping("/page/{pageNo}")
    public String page(Model model, @PathVariable Integer pageNo) {
        model.addAttribute("title", "文章列表");
        return this.queryArticleList(model, pageNo);
    }

    private String queryArticleList(Model model, Integer pageNo) {
        Page<Article> page = this.articleRepository.findByAudit(
                AuditEnum.PASSED,
                PageRequest.of(pageNo - 1, 10, Sort.Direction.DESC, "gatherDate")
        );
        model.addAttribute("page", page)
                .addAttribute("pageNo", pageNo);
        return "front/list";
    }


    /**
     * 内容页
     *
     * @param model model
     * @param id    文章ID
     * @return ID对应的文章
     */
    @GetMapping("/articles/{id}")
    public String article(Model model, @PathVariable Long id) {
        Optional<Article> articleOptional = this.articleRepository.findById(id);

        // 文章不存在
        if (!articleOptional.isPresent()) {
            return ConstantsApp.PAGE_NOT_FOUND;
        }

        Article articleInDB = articleOptional.get();

        // 如果该文章的审核状态不是通过状态，返回404
        if (!AuditEnum.PASSED.equals(articleInDB.getAudit())) {
            return ConstantsApp.PAGE_NOT_FOUND;
        }

        int click = articleInDB.getClick();
        articleInDB.setClick(click + 1);
        this.articleRepository.save(articleInDB);

//        Date gatherDate = articleInDB.getGatherDate();
        long gatherDate = articleInDB.getGatherDate().getTime();


        Article next = this.articleRepository.findByAuditAndGatherDateGreaterThan(
                AuditEnum.PASSED,
                gatherDate,
                PageRequest.of(0, 10, Sort.Direction.ASC, "gatherDate")
        ).getContent()
                .stream()
                .findFirst()
                .orElse(null);

        Article prev = this.articleRepository.findByAuditAndGatherDateLessThan(
                AuditEnum.PASSED,
                gatherDate,
                PageRequest.of(0, 10, Sort.Direction.DESC, "gatherDate")
        ).getContent()
                .stream()
                .findFirst()
                .orElse(null);

        model.addAttribute("article", articleInDB)
                .addAttribute("next", next)
                .addAttribute("prev", prev);

        return "front/article";
    }


    /**
     * 随机排序
     *
     * @param model model
     * @return 随机排序的文章
     */
    @GetMapping("/random")
    public String random(Model model) {
        NativeSearchQuery build = new NativeSearchQueryBuilder().withSort(
                new ScriptSortBuilder(
                        new Script("Math.random()"),
                        ScriptSortBuilder.ScriptSortType.NUMBER
                ).order(SortOrder.ASC)
        ).build();
        Page<Article> page = this.articleRepository.search(build);
        model.addAttribute("page", page)
                .addAttribute("pageNo", 1)
                .addAttribute("title", "文章列表");

        return "front/list";
    }
}
