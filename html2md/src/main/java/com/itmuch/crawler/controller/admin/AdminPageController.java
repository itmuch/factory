package com.itmuch.crawler.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminPageController {
    /**
     * 用户管理页面
     *
     * @return 用户管理页
     */
    @GetMapping("/users/index")
    public String list() {
        return "admin/users/index";
    }

    /**
     * 任务管理页面
     *
     * @return 任务管理页
     */
    @GetMapping("/jobs/index")
    public String jobs() {
        return "admin/jobs/index";
    }

    /**
     * 站点管理页面
     *
     * @return 站点管理页
     */
    @GetMapping("/sites/index")
    public String sites() {
        return "admin/sites/index";
    }

    /**
     * 文章管理页面
     *
     * @return 文章管理页
     */
    @GetMapping("/articles/index")
    public String articles() {
        return "admin/contents/articles/index";
    }
}
