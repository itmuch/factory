package com.itmuch.crawler.controller.front.user;

import com.itmuch.crawler.core.exception.BizRuntimeException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {
    @GetMapping("/login")
    public String login() {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            throw new BizRuntimeException(
                    500,
                    "该用户已登录",
                    "该用户已登录"
            );
        }
        return "front/login";
    }
}
