package com.itmuch.crawler.controller.admin.content;

import com.itmuch.crawler.core.constants.ConstantsCode;
import com.itmuch.crawler.core.convert.AjaxResult;
import com.itmuch.crawler.core.exception.BizRuntimeException;
import com.itmuch.crawler.core.page.PageVoWithSort;
import com.itmuch.crawler.domain.AuditEnum;
import com.itmuch.crawler.domain.content.Article;
import com.itmuch.crawler.repository.ArticleRepository;
import com.itmuch.crawler.util.mapper.BeanMapper;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.assertj.core.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/admin/articles")
public class ArticleAdminController {
    private final ElasticsearchTemplate elasticsearchTemplate;
    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleAdminController(ElasticsearchTemplate elasticsearchTemplate, ArticleRepository articleRepository) {
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.articleRepository = articleRepository;
    }

    @GetMapping("")
    public Page<Article> search(@RequestParam(required = false) String keyword,
                                @RequestParam(required = false) String audit,
                                PageVoWithSort pageVo
    ) {
        AuditEnum auditEnum = null;
        try {
            auditEnum = AuditEnum.valueOf(audit);
        } catch (Exception ignored) {
        }

        String escape = keyword == null ? null : QueryParser.escape(keyword);
        CriteriaQuery criteriaQuery = new CriteriaQuery(
                new Criteria("title").fuzzy(escape)
                        .or(new Criteria("content").fuzzy(escape))
                        .and(new Criteria("audit").is(auditEnum))
        ).setPageable(
                PageRequest.of(pageVo.getPage() - 1, pageVo.getRows(), pageVo.getSpringSort())
        );
        return this.elasticsearchTemplate.queryForPage(criteriaQuery,
                Article.class);
    }

    /**
     * 编辑文章
     *
     * @param article 文章
     * @return 编辑结果
     */
    @PutMapping("")
    public AjaxResult edit(@RequestBody @Validated Article article) {
        Optional<Article> byId = this.articleRepository.findById(article.getId());
        if (!byId.isPresent()) {
            throw new BizRuntimeException(
                    ConstantsCode.DATA_NOT_FOUND,
                    "该文章不存在",
                    "该文章不存在");
        }

        Article articleInDB = byId.get();
        // 目前只要经过人工编辑，就自动审核。防止这边在编辑，那边采集又覆盖掉。
        articleInDB.setAudit(AuditEnum.PASSED);
        BeanMapper.map(article, articleInDB);
        this.articleRepository.save(articleInDB);
        return new AjaxResult().success();
    }

    /**
     * 审核/拒绝审核
     *
     * @param audit 审核状态
     * @param ids   要审核的文章id
     * @return 操作结果
     */
    @PatchMapping("/{audit:passed|failed}")
    public AjaxResult audit(@PathVariable String audit, @RequestBody Long[] ids) {
        if (Arrays.isNullOrEmpty(ids)) {
            throw new BizRuntimeException(
                    ConstantsCode.DATA_NOT_FOUND,
                    "至少勾选1条",
                    "至少勾选1条");
        }
        Stream<Article> allByIdIn = this.articleRepository.findAllByIdIn(ids);
        List<Article> collect = allByIdIn
                .peek(t -> t.setAudit(AuditEnum.valueOf(audit.toUpperCase())))
                .collect(Collectors.toList());
        this.articleRepository.saveAll(collect);
        return new AjaxResult().success();
    }
}
