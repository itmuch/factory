package com.itmuch.crawler.controller.admin.job;

import com.google.common.collect.Maps;
import com.itmuch.crawler.core.page.PageVo;
import com.itmuch.crawler.core.constants.ConstantsCode;
import com.itmuch.crawler.core.convert.AjaxResult;
import com.itmuch.crawler.core.exception.BizRuntimeException;
import com.itmuch.crawler.domain.content.Site;
import com.itmuch.crawler.domain.job.JobConfiguration;
import com.itmuch.crawler.quartz.manager.JobManager;
import com.itmuch.crawler.repository.JobConfigurationRepository;
import com.itmuch.crawler.repository.SiteRepository;
import com.itmuch.crawler.util.mapper.BeanMapper;
import com.itmuch.crawler.util.mapper.JsonMapper;
import com.itmuch.crawler.webmagic.config.BlogCrawlerConfiguration;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

@RestController
@RequestMapping("/admin/jobs")
public class JobConfigurationAdminController {
    private final JobConfigurationRepository jobConfigurationRepository;
    private final ElasticsearchTemplate elasticsearchTemplate;
    private final SiteRepository siteRepository;
    private final JobManager jobManager;

    @Autowired
    public JobConfigurationAdminController(JobConfigurationRepository jobConfigurationRepository, ElasticsearchTemplate elasticsearchTemplate, SiteRepository siteRepository, JobManager jobManager) {
        this.jobConfigurationRepository = jobConfigurationRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.siteRepository = siteRepository;
        this.jobManager = jobManager;
    }


    @GetMapping("")
    public Page<JobConfiguration> search(String name, PageVo pageVo) {
        CriteriaQuery criteriaQuery = new CriteriaQuery(
                new Criteria().and(new Criteria("name").fuzzy(name))
        ).setPageable(
                PageRequest.of(pageVo.getPage() - 1, pageVo.getRows())
        );
        return this.elasticsearchTemplate.queryForPage(criteriaQuery,
                JobConfiguration.class);
    }

    /**
     * 添加job配置
     *
     * @param jobConfiguration job配置
     * @return job
     */
    @PostMapping("")
    public AjaxResult<JobConfiguration> add(@RequestBody @Validated JobConfiguration jobConfiguration) {
        // Site数据校验
        Site site = jobConfiguration.getSite();
        String url = site.getUrl();
        String urlHash = DigestUtils.md5Hex(url);
        Optional<Site> byId = this.siteRepository.findById(
                QueryParser.escape(url)
        );
        if (byId.isPresent()) {
            throw new BizRuntimeException(
                    ConstantsCode.DATA_ALREADY_EXISTS,
                    "该站点已存在",
                    "该站点已存在，无法再次添加");
        }

        // job配置校验
        Optional<JobConfiguration> config = this.jobConfigurationRepository.findById(urlHash);
        if (config.isPresent()) {
            throw new BizRuntimeException(
                    ConstantsCode.DATA_ALREADY_EXISTS,
                    "该Job已存在",
                    "该Job已存在，无法再次添加");
        }

        // job的参数校验
        String jobParam = jobConfiguration.getJobParam();
        if (StringUtils.isBlank(jobParam)) {
            throw new BizRuntimeException(
                    ConstantsCode.PARAMTER_ERROR_CODE,
                    "采集规则为空",
                    "采集规则为空");
        }
        BlogCrawlerConfiguration blogCrawlerConfiguration = JsonMapper
                .defaultMapper()
                .fromJson(jobParam, BlogCrawlerConfiguration.class);
        if (blogCrawlerConfiguration == null) {
            throw new BizRuntimeException(
                    ConstantsCode.PARAMTER_ERROR_CODE,
                    "参数无法正常反序列化",
                    "参数无法正常反序列化");
        }
        if (!url.equals(blogCrawlerConfiguration.getSiteUrl())) {
            throw new BizRuntimeException(
                    ConstantsCode.PARAMTER_ERROR_CODE,
                    "参数的URL与站点URL不匹配，请修改",
                    "参数的URL与站点URL不匹配，请修改");
        }

        // 添加Site
        site.setId(urlHash);
        site.setCreateTime(new Date());
        this.siteRepository.save(site);

        // 添加job配置
        jobConfiguration.setId(urlHash);
        jobConfiguration.setCreateTime(new Date());
        if (StringUtils.isBlank(jobConfiguration.getGroup())) {
            jobConfiguration.setGroup(JobConfiguration.JOB_DEFAULT_GROUP);
        }

        JobConfiguration data = this.jobConfigurationRepository.save(jobConfiguration);
        return new AjaxResult<JobConfiguration>().success(data);
    }

    /**
     * 修改job配置
     *
     * @param jobConfiguration job配置
     * @return job
     */
    @PutMapping("")
    public AjaxResult<JobConfiguration> edit(@RequestBody @Validated JobConfiguration jobConfiguration) {
        String id = jobConfiguration.getId();

        Optional<JobConfiguration> configurationOptional = this.jobConfigurationRepository.findById(id);
        if (!configurationOptional.isPresent())
            throw new BizRuntimeException(
                    ConstantsCode.DATA_NOT_FOUND,
                    "该Job不存在",
                    "该Job不存在");

        // 判断站点是否存在，如果不存在，不允许修改job配置
        Optional<Site> siteOptional = this.siteRepository.findById(id);
        if (!siteOptional.isPresent()) {
            throw new BizRuntimeException(
                    ConstantsCode.DATA_NOT_FOUND,
                    "该站点不存在",
                    "该站点不存在");
        }

        String jobParam = jobConfiguration.getJobParam();
        if (StringUtils.isBlank(jobParam)) {
            throw new BizRuntimeException(
                    ConstantsCode.PARAMTER_ERROR_CODE,
                    "采集规则为空",
                    "采集规则为空");
        }
        BlogCrawlerConfiguration blogCrawlerConfiguration = JsonMapper
                .defaultMapper()
                .fromJson(jobParam, BlogCrawlerConfiguration.class);
        if (blogCrawlerConfiguration == null) {
            throw new BizRuntimeException(
                    ConstantsCode.PARAMTER_ERROR_CODE,
                    "参数无法正常反序列化",
                    "参数无法正常反序列化");
        }
        if (!jobConfiguration.getSite().getUrl().equals(blogCrawlerConfiguration.getSiteUrl())) {
            throw new BizRuntimeException(
                    ConstantsCode.PARAMTER_ERROR_CODE,
                    "参数的URL与站点URL不匹配，请修改",
                    "参数的URL与站点URL不匹配，请修改");
        }

        // TODO: 2017/11/6 这里可能存在事务问题，暂不考虑。
        // 修改job配置
        JobConfiguration configInDB = configurationOptional.get();
        BeanMapper.map(jobConfiguration, configInDB);
        configInDB.setUpdateTime(new Date());
        JobConfiguration data = this.jobConfigurationRepository.save(configInDB);

        // 修改站点
        Site siteInDB = siteOptional.get();
        BeanMapper.map(jobConfiguration.getSite(), siteInDB);
        siteInDB.setUpdateTime(new Date());

        this.siteRepository.save(siteInDB);

        return new AjaxResult<JobConfiguration>()
                .success(data);
    }


    @PatchMapping("/{id}/{status:pause|resume}")
    public AjaxResult pause(@PathVariable String id, @PathVariable String status) throws SchedulerException {
        Optional<JobConfiguration> optional = this.jobConfigurationRepository.findById(id);
        if (!optional.isPresent()) {
            throw new BizRuntimeException(
                    ConstantsCode.DATA_NOT_FOUND,
                    "该Job不存在",
                    "该Job不存在");
        }

        JobConfiguration jobConfiguration = optional.get();

        // 只操作quartz，不操作数据库。数据库的状态由定时任务同步。这样比较简单。
        // 否则两边都操作的话，很麻烦；而且这里：执行resume操作后，可能会变成WAITING，也可能变成BLOCKED，无法确认状态。
        switch (status) {
            case "pause": {
                this.jobManager.pauseJob(jobConfiguration);
                break;
            }
            case "resume": {
                this.jobManager.resumeJob(jobConfiguration);
            }
        }
        return new AjaxResult<JobConfiguration>()
                .success();
    }

    /**
     * 通过job参数的类名，生成相应的方法模板。
     *
     * @param clazz 参数的类名
     * @return 模板
     */
    @GetMapping("/gen-param-template")
    public Object show(@RequestParam String clazz) {
        try {
            Class<?> c = Class.forName(clazz);
            return c.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            HashMap<Object, Object> map = Maps.newHashMap();
            map.put("error", "发生异常" + e.getMessage());
            return map;
        }
    }
}
