package com.itmuch.crawler.controller.admin.site;

import com.itmuch.crawler.core.page.PageVo;
import com.itmuch.crawler.domain.content.Site;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * site不允许直接操作，site的操作都集成是job configuration中了。
 */
@RestController
@RequestMapping("/admin/sites")
public class SiteAdminController {
    private final ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    public SiteAdminController(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @GetMapping("")
    public Page<Site> search(String name, PageVo pageVo) {
        CriteriaQuery criteriaQuery = new CriteriaQuery(
                new Criteria().and(new Criteria("username").fuzzy(name))
        ).setPageable(
                PageRequest.of(pageVo.getPage() - 1, pageVo.getRows())
        );
        return this.elasticsearchTemplate.queryForPage(criteriaQuery,
                Site.class);
    }
}
