package com.itmuch.crawler.controller.admin.user;

import com.itmuch.crawler.core.page.PageVo;
import com.itmuch.crawler.core.convert.AjaxResult;
import com.itmuch.crawler.core.exception.BizRuntimeException;
import com.itmuch.crawler.domain.user.User;
import com.itmuch.crawler.repository.user.UserRepository;
import com.itmuch.crawler.util.mapper.BeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/admin/users")
public class UserAdminController {
    private final UserRepository userRepository;
    private final ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    public UserAdminController(UserRepository userRepository, ElasticsearchTemplate elasticsearchTemplate) {
        this.userRepository = userRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @GetMapping("")
    public Page<User> search(String username, PageVo pageVo) {
        CriteriaQuery criteriaQuery = new CriteriaQuery(
                new Criteria().and(new Criteria("username").fuzzy(username))
        ).setPageable(
                PageRequest.of(pageVo.getPage() - 1, pageVo.getRows())
        );
        return this.elasticsearchTemplate.queryForPage(criteriaQuery,
                User.class);
    }

    @PutMapping("")
    public AjaxResult edit(@RequestBody User user) {
        Long id = user.getId();
        if (id == null) {
            throw new BizRuntimeException(
                    400,
                    "id为空",
                    "id为空"
            );
        }
        Optional<User> userOptional = this.userRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new BizRuntimeException(
                    400,
                    "该用户不存在",
                    "该用户不存在"
            );
        }
        User userInDB = userOptional.get();
        BeanMapper.map(user, userInDB);
        this.userRepository.save(userInDB);
        return new AjaxResult().success();
    }
}
