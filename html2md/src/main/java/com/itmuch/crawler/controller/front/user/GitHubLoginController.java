package com.itmuch.crawler.controller.front.user;

import com.itmuch.crawler.core.constants.ConstantsApp;
import com.itmuch.crawler.domain.user.User;
import com.itmuch.crawler.repository.user.UserRepository;
import com.itmuch.crawler.security.github.GitHubApi;
import com.itmuch.crawler.security.jwt.JwtToken;
import com.itmuch.crawler.security.jwt.TokenUtils;
import com.itmuch.crawler.util.mapper.JsonMapper;
import com.itmuch.crawler.util.snowflake.IDGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.LinkedHashMap;

@Controller
@Slf4j
@RequestMapping("/login")
public class GitHubLoginController {
    private final GitHubApi gitHubApi;
    private final TokenUtils tokenUtils;
    private final UserRepository userRepository;

    @Autowired
    public GitHubLoginController(GitHubApi gitHubApi, TokenUtils tokenUtils, UserRepository userRepository) {
        this.gitHubApi = gitHubApi;
        this.tokenUtils = tokenUtils;
        this.userRepository = userRepository;
    }

    /**
     * GitHub登录页，访问该URL会跳转到GitHub进行授权
     *
     * @return GitHub授权页
     * @throws OAuthSystemException 异常
     */
    @GetMapping({"/github"})
    public String login() throws OAuthSystemException {
        return "redirect:" + gitHubApi.tryLogin();
    }

    @GetMapping("/github/callback")
    public String callback(String code) {
        // 拿到github的userInfo
        OAuthResourceResponse userInfo;
        try {
            userInfo = this.gitHubApi.getUserInfo(code);
        } catch (OAuthSystemException | OAuthProblemException e) {
            throw new IllegalArgumentException("GitHub回调页面认证失败");
        }
        LinkedHashMap map = JsonMapper.defaultMapper()
                .fromJson(userInfo.getBody(), LinkedHashMap.class);
        Integer gitHubId = (Integer) map.get("id");


        User userInDB = this.userRepository.findByGitHubId(gitHubId);
        // 如该用户不存在，注册
        if (null == userInDB) {
            String loginName = (String) map.get("login");
            String homepage = (String) map.get("blog");
            String avatar = (String) map.get("avatar_url");
            String bio = (String) map.get("bio");
            String role = gitHubId == 9031050 ? ConstantsApp.Role.ADMIN : ConstantsApp.Role.USER;
            userInDB = this.userRepository.save(
                    User.builder()
                            .id(IDGenerator.genId())
                            .gitHubId(gitHubId)
                            .role(role)
                            .username(loginName)
                            .homepage(homepage)
                            .avatar(avatar)
                            .bio(bio)
                            .build()
            );
        }
        // 生成token
        String token = this.tokenUtils.generateToken(userInDB.getUsername());
        // 登录
        Subject subject = SecurityUtils.getSubject();
        subject.login(new JwtToken(userInDB.getUsername(), token, true));

        // 设置session
        Session session = subject.getSession(true);
        session.setAttribute(ConstantsApp.CURRENT_USER, userInDB);
        log.info("设置session，User = {}", userInDB);
        return "redirect:/";
    }
}
