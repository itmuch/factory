package com.itmuch.crawler.webmagic.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class HtmlUtil {
    /**
     * 清除hexo的html代码块中的无用标签
     *
     * @param originContent 原始HTML内容
     * @return 清除后的内容
     */
    public static String clean(String originContent, String url) {
        Html html = new Html(originContent);
        log.debug("clean...url = {}", url);
//        if (!"http://www.itmuch.com/install/kafka-cluster/".equals(url)) {
//            return "";
//        }
        html.getDocument()
                .outputSettings()
                .prettyPrint(false);
        Selectable highlight = html.xpath("//figure[@class='highlight']");
        List<String> figures = highlight.all();

        String content = originContent;
        // 如果用的是highlight.js，例如：hexo
        //<figure class="highlight yaml">
        //    <table>
        //        <tr>
        //            <td class="gutter">
        //                <pre>
        //                    <div class="line">1</div>
        //                    <div class="line">2</div>
        //                    <div class="line">3</div>
        //                    <div class="line">4</div>
        //                </pre>
        //            </td>
        //            <td class="code">
        //                <pre>
        //                    <div class="line"><span class="attr">eureka:</span></div>
        //                    <div class="line"><span class="attr">  client:</span></div>
        //                    <div class="line"><span class="attr">    serviceUrl:</span></div>
        //                    <div class="line"><span class="attr">      defaultZone:</span> <span class="attr">http://localhost:8761/eureka/</span></div>
        //                </pre>
        //            </td>
        //        </tr>
        //    </table>
        //</figure>
        if (CollectionUtils.isNotEmpty(figures)) {
            for (String figure : figures) {
                Html figureHtml = new Html(figure);
                Document document = figureHtml.getDocument();
                document.outputSettings()
                        .prettyPrint(false);

                // 获得figure元素的class属性
                String attr = document.select("figure")
                        .first()
                        .attr("class");

                String languageName = HtmlUtil.getLanguageName(attr);

                StringBuilder cleanedCode = new StringBuilder("<pre><code class=\"" + languageName + "\">");
                // 每一个DIV对应于代码的一行
                List<String> lines = figureHtml.xpath("//td[@class='code']/pre/div[@class='line']").all();
                // div标签
                for (String line : lines) {
                    // 正则替换掉没用的HTML元素，只保留代码内容
                    // TODO 可能存在问题：如果内容本身就是html呢？？？例如<div> html </div>
                    String s = line.replaceAll("<(.)+?>", "");
                    cleanedCode.append(s)
                            // 换行
                            .append("\n");
                }
                cleanedCode.append("</pre></code>");
                content = content.replace(figure, cleanedCode);
            }
            return content;
        }

        // 如果用的是UEditor
        Selectable brush = html.xpath("//pre[@*]");
        List<String> pres = brush.all();
        if (CollectionUtils.isNotEmpty(pres)) {

            for (String pre : pres) {
                Html preHtml = new Html(pre);
                Document preDocument = preHtml.getDocument();
                preDocument.outputSettings()
                        .prettyPrint(false);

                // 获得pre元素的class属性
                String attr = preDocument.select("pre")
                        .first()
                        .attr("class");

                String languageName = HtmlUtil.getLanguageName(attr);
                StringBuilder cleanedCode = new StringBuilder("<pre><code class=\"" + languageName + "\">");

                String code;
                // 获取code段的内容 有的网站代码形如：<pre><code>...</code></pre>
                String code1 = preDocument.select("code").html();
                // 获取pre段的内容 有的网站代码形如：<pre>...</pre>
                String pre1 = preDocument.select("pre").html();
                code = StringUtils.isNotBlank(code1) ? code1 : pre1;
                // 正则替换掉没用的HTML元素，只保留代码内容
                // TODO 可能存在问题：如果内容本身就是html呢？？？例如<div> html </div>
                code = code.replaceAll("<(.)+?>", "");
                cleanedCode.append(code);
                cleanedCode.append("</pre></code>");
                content = content.replace(pre, cleanedCode);
            }
            return content;
        }
        return content;
    }

    /**
     * 从字符串中，获取语言名称
     *
     * @param string 字符串
     * @return 语言名称
     */
    private static String getLanguageName(String string) {
        String preClass = "";
        List<String> strings = HtmlUtil.sortLanguagesDESC();
        for (String language : strings) {
            if (string.toLowerCase().contains(language)) {
                preClass = language;
                break;
            }
        }
        return preClass;
    }

    public static String completeTheURL(String content, String url) {
        content = HtmlUtil.completeTheURL(content, url, "a", "href");
        content = HtmlUtil.completeTheURL(content, url, "img", "src");
        return content;
    }

    /**
     * 补全不完整的URL，有时候采集过来的地址是/xxx/img.png，这个时候需要加上站点的URL。
     * 参考：http://blog.csdn.net/xcy13638760/article/details/21163885
     *
     * @param content  内容
     * @param url      URL
     * @param tag      标签，例如img、a
     * @param property 标签的属性，例如src、href等
     * @return 换成绝对URL后的内容
     */
    private static String completeTheURL(String content, String url, String tag, String property) {
        String newContent = "";
        try {
            if (StringUtils.isNotBlank(content)) {
                URI uri = new URI(url);
                Document doc = Jsoup.parseBodyFragment(content);
                doc.outputSettings()
                        .prettyPrint(false);
                Elements elementsByTag = doc.getElementsByTag(tag);
                for (Element element : elementsByTag) {
                    // 通过属性查到的地址
                    String elePropValue = element.attr(property);
                    // 如果不以http://、https://、ftp://或#开头，说明是个不完整的地址
                    if (!elePropValue.matches("^(https?|http?|ftp):(\\\\|//).*$"))
                        if (StringUtils.isNotBlank(elePropValue) && !elePropValue.startsWith("#")) {
                            String baseURL;
                            // 绝对路径
                            if (elePropValue.startsWith("/")) {
                                baseURL = uri.getScheme() + "://" + uri.getHost();
                            }
                            //相对路径
                            else {
                                baseURL = url + "/";
                            }
                            String fullURL = baseURL + elePropValue;
                            element.attr(property, fullURL);
                            log.debug("uri = {}, 原始地址：{}, 处理后的地址：{}", uri.toString(), elePropValue, fullURL);
                        }
                }
                newContent = doc.body()
                        .html();
            }
        } catch (URISyntaxException e) {
            log.error("发生异常", e);
        }
        return newContent;
    }

    /**
     * 通过HTML获取其文本
     * 任意html，残缺不全也可以
     * 参考：https://www.cnblogs.com/cnsevennight/p/4468055.html
     *
     * @param html HTML
     * @return 文本
     */
    private static String getText(String html) {
        //<.*?>为正则表达式，其中的.表示任意字符，*?表示出现0次或0次以上，此方法可以去掉双头标签(双头针对于残缺的标签)
        //"<.*?"表示<尖括号后的所有字符，此方法可以去掉残缺的标签，及后面的内容
        //" "，若有多种此种字符，可用同一方法去除
        html = html.replaceAll("<.*?>", "  ")
                .replaceAll(" ", " ");
        html = html.replaceAll("<.*?", "");
        return (html + "...");
    }

    /**
     * 截取长度的html的摘要
     *
     * @param html   HTML
     * @param length HTML长度
     * @return 摘要
     */
    public static String genSummary(String html, int length) {
        if (html.length() < length) {
            return getText(html);
        }
        return getText(
                html.substring(0, length)
        );
    }

    /**
     * 将语言列表倒序排列
     *
     * @return 倒序排列的语言数组
     */
    private static List<String> sortLanguagesDESC() {
        List<String> languages = Lists.newArrayList("apache", "bash", "coffeescript", "cpp",
                "cs", "css", "diff", "html", "http",
                "ini", "java", "javascript", "json",
                "makefile", "markdown", "nginx", "objectivec",
                "perl", "php", "python", "ruby", "shell",
                "sql", "vbscript", "xhtml", "xml", "yaml", "yml", "go"
        );
        return languages.stream()
                // 按照长度倒序排列
                .sorted((o1, o2) -> o2.length() - o1.length())
                .collect(Collectors.toList());
    }
}
