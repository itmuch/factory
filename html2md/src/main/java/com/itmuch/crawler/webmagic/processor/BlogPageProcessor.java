package com.itmuch.crawler.webmagic.processor;

import com.itmuch.crawler.constant.ArticleConstant;
import com.itmuch.crawler.domain.AuditEnum;
import com.itmuch.crawler.domain.content.Article;
import com.itmuch.crawler.domain.content.Tag;
import com.itmuch.crawler.util.DateTimeUtil;
import com.itmuch.crawler.webmagic.config.BlogCrawlerConfiguration;
import com.itmuch.crawler.webmagic.util.HtmlUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 参考：http://www.jianshu.com/p/c3fc3129407d
 * 博客爬虫processor
 */
@Slf4j
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BlogPageProcessor implements PageProcessor {
    private final Site site = Site.me()
            .setRetryTimes(3)
            .setSleepTime(1000)
            .setTimeOut(10000);

    private BlogCrawlerConfiguration blogCrawlerConfiguration;

    private boolean isList(Html html, String listPageXPath, String articlePageXPath) {
        String listPageElement;
        if (StringUtils.isNotBlank(listPageXPath)) {
            listPageElement = html.xpath(listPageXPath).get();
            return StringUtils.isNotBlank(listPageElement);
        }
        // 如果没配置列表页xpath
        else {
            // 如果配置了文章页的xpath规则，则列表页必须不能包含文章页xpath元素。
            if (StringUtils.isBlank(articlePageXPath)) {
                throw new IllegalArgumentException("page、article的xpath均未配置，无法区分列表页、内容页。");
            }
            // 如果配置了内容页的xpath
            String contentPageElement;
            contentPageElement = html.xpath(articlePageXPath).get();
            // 如果该页不包含文章页的元素
            return StringUtils.isBlank(contentPageElement);
        }
    }


    @Override
    public void process(Page page) {
        Html html = page.getHtml();
        // 如果有该元素，则是列表页。
        String listPageXPath = this.blogCrawlerConfiguration.getListPageXPath();

        // 如果有该元素，则是内容页。
        String articlePageXPath = this.blogCrawlerConfiguration.getArticlePageXPath();

        if (this.isList(html, listPageXPath, articlePageXPath)) {
            // 添加内容页的URL
            String listPageURLXPath = this.blogCrawlerConfiguration.getListPageURLXPath();
            List<String> contentUrl = html.xpath(listPageURLXPath)
                    .links()
                    .all();
            page.addTargetRequests(contentUrl);
            // 对于列表页，无需存储，故而设为skip
            log.debug("丢弃列表页中的URL = {}", page.getUrl());
            page.setSkip(true);
        }
        // 内容页
        else {
            // 一、禁止JSoup的格式化，否则可能会导致文章中的代码格式有问题。
            html.getDocument()
                    .outputSettings()
                    .prettyPrint(false);

            // 二、构成文章的必选组成部分：title、URL、urlHash、content、summary。
            // 这部分不做blogCrawlerConfiguration中的几个field是否存在的判断了，直接用。
            // title
            String title = html.xpath(this.blogCrawlerConfiguration.getArticleTitleXPath()).toString();

            // url
            String url = page.getUrl().toString();
            // url hash
            String hash = DigestUtils.md5Hex(url);
            // 采集过来的原始的文章内容
            String originContent = html.xpath(this.blogCrawlerConfiguration.getArticleContentXPath()).toString();

            // 如果如下任意一个为空，就skip
            if (StringUtils.isBlank(title)
                    || StringUtils.isBlank(originContent)
                    ) {
                log.error("内容页数据不完整，请检查。URL = {}，title = {}， originContent = {}",
                        page.getUrl(),
                        title,
                        originContent
                );
                page.setSkip(true);
                return;
            }

            String content = originContent;

            // 如果需要删除一些元素
            List<String> articleDeleteXPaths = this.blogCrawlerConfiguration.getArticleDeleteXPaths();

            if (CollectionUtils.isNotEmpty(articleDeleteXPaths)) {
                for (String item : articleDeleteXPaths) {
                    String contentToDelete = html.xpath(item).toString();
                    if (contentToDelete != null) {
                        content = content.replace(contentToDelete, "");
                    }
                }
            }

            // 清理代码里没用的元素，主要是对<pre>标签进行处理
            content = HtmlUtil.clean(content, url);
            // 补全不完整的URL
            content = HtmlUtil.completeTheURL(content, url);

            // 摘要
            String summary = HtmlUtil.genSummary(content, 300);
//            String plainContent = Jsoup.parse(content)
//                    .body()
//                    .text();
//            int contentLength = plainContent.length();
//            String summary = plainContent.substring(0,
//                    contentLength > 200 ? 200 : contentLength
//            );
//            summary = StringEscapeUtils.escapeHtml(summary);

            // 三、处理文章的可选组成部分：这部分由于可能不存在，所以可能连xpath表达式都没有设置。因此需要判断一下。
            // 文章分类
            String articleCategoryXPath = this.blogCrawlerConfiguration.getArticleCategoryXPath();
            String category = null;
            if (StringUtils.isNotBlank(articleCategoryXPath)) {
                category = html.xpath(articleCategoryXPath).toString();
            }

            // 标签
            String articleTagXPath = this.blogCrawlerConfiguration.getArticleTagXPath();
            List<String> tags = null;
            if (StringUtils.isNotBlank(articleTagXPath)) {
                tags = html.xpath(articleTagXPath).all()
                        .stream()
                        .map(String::trim)
                        .collect(Collectors.toList());
            }

            // 发布时间
            String articleIssueDateXPath = this.blogCrawlerConfiguration.getArticleIssueDateXPath();
            Date issueDate = null;
            if (StringUtils.isNotBlank(articleIssueDateXPath)) {
                String stringDate = html.xpath(articleIssueDateXPath).toString();
                issueDate = DateTimeUtil.parseToDate(stringDate);
            }

            // 四、构建article并put
            Article article = Article.builder()
                    .siteId(this.blogCrawlerConfiguration.getSiteId())
                    .audit(AuditEnum.NOT_YET)
                    .category(category)
                    .tags(tags)
                    .title(title)
                    .url(url)
                    .hash(hash)
                    .issueDate(issueDate)
                    .gatherDate(new Date())
                    .summary(summary)
                    .content(content)
                    .click(0)
                    .build();
            page.putField(ArticleConstant.ARTICLE, article);

            // 构建tag并put
            if (CollectionUtils.isNotEmpty(tags)) {
                List<Tag> tagObjects = tags.stream()
                        .map(t -> Tag.builder()
                                .name(t)
                                .build()
                        )
                        .collect(Collectors.toList());
                page.putField(ArticleConstant.TAG_OBJECTS, tagObjects);
            }
        }
    }

    @Override
    public Site getSite() {
        return this.site;
    }
}
