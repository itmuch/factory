package com.itmuch.crawler.webmagic.processor;

import com.google.common.collect.Lists;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.JsonFilePipeline;
import us.codecraft.webmagic.processor.PageProcessor;

import java.util.ArrayList;
import java.util.List;

// TODO 参考：http://www.jianshu.com/p/c3fc3129407d    爬简书、整合spring boot
public class CSDNBlogPageProcessor implements PageProcessor {
    private Site site = Site.me().setRetryTimes(3).setSleepTime(1000).setTimeOut(10000);

    public static void main(String[] args) {
        Spider.create(new CSDNBlogPageProcessor())
                .addUrl("http://blog.csdn.net?&page=1")
                .addPipeline(new JsonFilePipeline("/Users/zhouli/Desktop/csdn"))
                //开启5个线程抓取
                .thread(5)
                //启动爬虫
                .run();
    }

    @Override
    public void process(Page page) {
        // 列表页
        if (!page.getUrl().regex("http://blog\\.csdn\\.net/(\\w+)/article/details/\\d+").match()) {
            // 添加其他列表页，只取前五页。
            ArrayList<String> list = Lists.newArrayList();
            for (int i = 1; i <= 5; i++) {
                list.add("?&page=" + i);
            }
            page.addTargetRequests(list);

            List<String> all = page.getHtml()
                    .xpath("//h3[@class=csdn-tracking-statistics]")
                    .links()
                    .all();
            page.addTargetRequests(
                    all
            );
            page.setSkip(true);
        }
        // 内容页
        else {
            page.putField("url",
                    page.getUrl().all().toString());
            page.putField("name",
                    page.getHtml().xpath("//span[@class='link_title']/a/text()").all().toString());
            page.putField("topics",
                    page.getHtml().xpath("//span[@class='link_categories']/a/text()").all());
        }
    }

    @Override
    public Site getSite() {
        return this.site;
    }
}
