package com.itmuch.crawler.webmagic.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 博客爬虫配置项
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BlogCrawlerConfiguration {

    /**
     * 站点地址
     */
    private String siteUrl = "http://www.itmuch.com";

    /**
     * 站点id
     *
     * @see com.itmuch.crawler.domain.content.Site#id
     */
    private String siteId;

    /**
     * 列表页的xpath语句，用于确定是否列表页。如果有该元素，则是列表页。listPageXPath、articlePageXPath 两者至少写其一
     */
    private String listPageXPath = "//div[@class='home-post-head']";

    /**
     * 内容页的xpath语句，用于确定是否文章页。如果有该元素，则是文章页。listPageXPath、articlePageXPath 两者至少写其一
     */
    private String articlePageXPath = "//div[@class='aaa-bbb-ccccc']";

    /**
     * 列表页中要采集的URL的xpath
     */
    private String listPageURLXPath = "//div[@class='post-content']|//div[@id='page-nav]";

    /**
     * 文章title的xpath
     */
    @NotBlank
    private String articleTitleXPath = "//div[@class='post-head']/h1/text()";

    /**
     * 文章category的xpath
     */
    private String articleCategoryXPath = "//span[@class='categories-meta']/span/text()";

    /**
     * 文章tag的xpath
     */
    private String articleTagXPath = "//span[@class='tags-meta']/text()";

    /**
     * 文章issueDate的xpath
     */
    private String articleIssueDateXPath = "//span[@class='date-meta']/text()";

    /**
     * 文章内容的xpath
     */
    @NotBlank
    private String articleContentXPath = "//div[@class='post-body']/html()";

    /**
     * 文章内容中需要删除元素的XPath列表
     */
    private List<String> articleDeleteXPaths;
}
