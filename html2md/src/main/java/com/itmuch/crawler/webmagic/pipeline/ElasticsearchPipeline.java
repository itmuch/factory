package com.itmuch.crawler.webmagic.pipeline;

import com.itmuch.crawler.constant.ArticleConstant;
import com.itmuch.crawler.domain.AuditEnum;
import com.itmuch.crawler.domain.content.Article;
import com.itmuch.crawler.repository.ArticleRepository;
import com.itmuch.crawler.repository.TagRepository;
import com.itmuch.crawler.util.snowflake.IDGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class ElasticsearchPipeline implements Pipeline {
    private final ArticleRepository articleRepository;
    private final TagRepository tagRepository;

    @Autowired
    public ElasticsearchPipeline(ArticleRepository savRepository, TagRepository tagRepository) {
        this.articleRepository = savRepository;
        this.tagRepository = tagRepository;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void process(ResultItems resultItems, Task task) {
        Map<String, Object> map = resultItems.getAll();
        Article article = (Article) map.get(ArticleConstant.ARTICLE);
        List tags = (List) map.get(ArticleConstant.TAG_OBJECTS);

        log.debug("数据准备完毕，article = {}, tags = {}", article, tags);

        // 保存Tag，目前没什么用。
        // TODO 以后做Tag列表（展示所有的Tag）的时候用
        if (CollectionUtils.isNotEmpty(tags)) {
            this.tagRepository.saveAll(tags);
        }
        this.saveArticle(article);
    }

    /**
     * 这里，必须加上synchronized关键字，否则：
     * 在article.setId(IDGenerator.genId());这一行有可能出现并发问题：即：两article设入了相同的id，继而导致文章插入条目不准确
     *
     * @param article 文章
     */
    private synchronized void saveArticle(Article article) {
        // 不应该通过findByHashAndTitle查询，因为对于一个URL，可能其文章title会改名
        // 改名后，正好再次触发采集。此时，如果查不到数据，就会再次插入一条数据
        Article articleInDB = this.articleRepository.findByHash(article.getHash());

        if (articleInDB != null) {
            // 如果文章已经审核通过，那么什么都不干
            if (AuditEnum.PASSED.equals(articleInDB.getAudit())) {
                return;
            }
            // 已经存在，则修改文章
            article.setId(articleInDB.getId());
        } else {
            // 生成id
            article.setId(IDGenerator.genId());
        }
        this.articleRepository.save(article);
    }
}
