package com.itmuch.crawler.constant;

public class ArticleConstant {

    /**
     * tag
     */
    public static final String TAG_OBJECTS = "tagList";

    /**
     * 文章
     */
    public static final String ARTICLE = "article";
}
