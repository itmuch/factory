package com.itmuch.crawler.security.github;

import lombok.extern.slf4j.Slf4j;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.GitHubTokenResponse;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Component;

@Component
@CacheConfig(cacheNames = "github")
@Slf4j
public class GitHubApi {
//    private static String gitHubUserUrl = "https://api.github.com/search/users" +
//            "?q=location:China&page={pageNo}" +
//            "&per_page={pageSize}" +
//            "&client_id=abebd06cffa78004ece0" +
//            "&client_secret=61f5f907bff766b4b25a13ba5e4c71feb157eebc";

    private final GitHubProperties gitHubProperties;

    @Autowired
    public GitHubApi(GitHubProperties gitHubProperties) {
        this.gitHubProperties = gitHubProperties;
    }

    public String tryLogin() throws OAuthSystemException {
        OAuthClientRequest oauthResponse = OAuthClientRequest
                .authorizationLocation(gitHubProperties.getUserAuthorizationUri())
                .setResponseType(OAuth.OAUTH_CODE)
                .setClientId(gitHubProperties.getClientId())
                .setRedirectURI(gitHubProperties.getRedirectUri())
                .setScope("scope")
                .buildQueryMessage();
        return oauthResponse.getLocationUri();
    }


    public OAuthResourceResponse getUserInfo(String code) throws OAuthSystemException, OAuthProblemException {
        OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());

        OAuthClientRequest accessTokenRequest = OAuthClientRequest
                .tokenLocation(gitHubProperties.getAccessTokenUri())
                .setGrantType(GrantType.AUTHORIZATION_CODE)
                .setClientId(gitHubProperties.getClientId())
                .setClientSecret(gitHubProperties.getClientSecret())
                .setCode(code)
                .setRedirectURI(gitHubProperties.getRedirectUri())
                .buildQueryMessage();

        GitHubTokenResponse tokenResponse = oAuthClient.accessToken(accessTokenRequest, GitHubTokenResponse.class);

        String accessToken = tokenResponse.getAccessToken();

        // Long expiresIn = tokenResponse.getExpiresIn();

        OAuthClientRequest userInfoRequest = new OAuthBearerClientRequest(gitHubProperties.getUserInfoUri())
                .setAccessToken(accessToken).buildQueryMessage();

        return oAuthClient.resource(userInfoRequest, OAuth.HttpMethod.GET, OAuthResourceResponse.class);
    }
}
