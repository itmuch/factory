package com.itmuch.crawler.security.github;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "oauth.github")
@Data
@Configuration
public class GitHubProperties {
    private String clientId;
    private String clientSecret;
    private String accessTokenUri;
    private String userAuthorizationUri;
    private String userInfoUri;
    private String redirectUri;
}
