package com.itmuch.crawler.security.jwt;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.shiro.authc.RememberMeAuthenticationToken;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JwtToken implements RememberMeAuthenticationToken {
    private String principal;
    private String token;
    private boolean rememberMe = false;

    @Override
    public String getPrincipal() {
        return this.principal;
    }

    @Override
    public Object getCredentials() {
        return this.token;
    }

    @Override
    public boolean isRememberMe() {
        return this.rememberMe;
    }
}
