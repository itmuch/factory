package com.itmuch.crawler.security.shiro;

import com.itmuch.crawler.domain.user.User;
import com.itmuch.crawler.repository.user.UserRepository;
import com.itmuch.crawler.security.jwt.TokenUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings("unused")
@Component
public class SubjectUtil {
    private final UserRepository userRepository;
    private final TokenUtils tokenUtils;

    @Autowired
    public SubjectUtil(UserRepository userRepository, TokenUtils tokenUtils) {
        this.userRepository = userRepository;
        this.tokenUtils = tokenUtils;
    }

    public Long getSubjectId() {
        String username = (String) SecurityUtils.getSubject().getPrincipal();
        if (StringUtils.isBlank(username)) {
            throw new IllegalArgumentException("该用户未登录");
        }

        User user = this.userRepository.findByUsername(username);
        if (user == null) {
            throw new IllegalArgumentException("该用户不存在");
        }
        return user.getId();
    }


    /**
     * 直接从token获取用户id，不抛异常。
     *
     * @param token token
     * @return 用户id
     */
    public Long getSubjectIdFromToken(String token) {
        if (StringUtils.isBlank(token)) {
            return null;
        }
        String usernameFromToken = this.tokenUtils.getUsernameFromToken(token);
        if (StringUtils.isBlank(usernameFromToken)) {
            return null;
        }
        User user = this.userRepository.findByUsername(usernameFromToken);
        if (user == null) {
            return null;
        }
        return user.getId();
    }
}
