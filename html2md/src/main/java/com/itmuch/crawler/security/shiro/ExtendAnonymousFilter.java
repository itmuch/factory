package com.itmuch.crawler.security.shiro;

import com.itmuch.crawler.core.constants.ConstantsApp;
import com.itmuch.crawler.domain.user.User;
import com.itmuch.crawler.repository.user.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AnonymousFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

@Slf4j
@AllArgsConstructor
public class ExtendAnonymousFilter extends AnonymousFilter {
    private UserRepository userRepository;

    /**
     * 解决remember me时，session过期的问题：
     * eg：remember me = 7天，session有效期30分钟，一个用户登录后，过了1天没有操作，那session就失效了。
     * 此时，就会导致session拿不到的问题。应该重新设置一下。
     * 参考：remember me设置session：http://blog.csdn.net/nsrainbow/article/details/36945267/
     *
     * @param request     请求
     * @param response    响应
     * @param mappedValue mappedValue
     * @return true
     */
    @SuppressWarnings("Duplicates")
    @Override
    protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) {
        Subject subject = SecurityUtils.getSubject();
        //如果isAuthenticated=false，证明不是登录过的，
        // isRemembered=true证明是没登陆直接通过记住我功能进来的
        if (!subject.isAuthenticated() && subject.isRemembered()) {
            Session session = subject.getSession(true);
            if (null == session.getAttribute(ConstantsApp.CURRENT_USER)) {
                // 设置session
                String username = subject.getPrincipal().toString();
                User user = this.userRepository.findByUsername(username);
                if (user != null) {
                    session.setAttribute(ConstantsApp.CURRENT_USER, user);
                    log.info("Remember Me的用户设置session, {}", user);
                }
            }
        }
        return super.onPreHandle(request, response, mappedValue);
    }
}
