package com.itmuch.crawler.security.shiro;

import com.itmuch.crawler.core.constants.ConstantsApp;
import com.itmuch.crawler.core.convert.AjaxResult;
import com.itmuch.crawler.domain.user.User;
import com.itmuch.crawler.repository.user.UserRepository;
import com.itmuch.crawler.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * authc Filter
 * 参考：shiro用ajax方式登录：http://blog.csdn.net/u013632755/article/details/51485158
 */
@Slf4j
public class ExtendFormAuthenticationFilter extends FormAuthenticationFilter {
    private UserRepository userRepository;

    public ExtendFormAuthenticationFilter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        if (isLoginRequest(request, response)) {
            if (isLoginSubmission(request, response)) {
                log.debug("Login submission detected.  Attempting to execute login.");
                return executeLogin(request, response);
            } else {
                log.debug("Login page view.");
                //allow them to see the login page ;)
                return true;
            }
        } else {
            // 不是ajax请求
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;

            if (!isAjax(httpServletRequest)) {
                log.debug("Attempting to access a path which requires authentication.  Forwarding to the " +
                        "Authentication url [" + getLoginUrl() + "]");

                super.saveRequestAndRedirectToLogin(request, response);
                return false;
            }
            // 是ajax请求
            else {
                JsonUtil.writeJson(
                        response,
                        new AjaxResult<>().fail(401, "禁止访问", "禁止访问")
                );
                return false;
            }
        }
    }

    @Override
    protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        Session session = subject.getSession(true);

        // 设置session
        String username = subject.getPrincipal().toString();
        User user = this.userRepository.findByUsername(username);
        session.setAttribute(ConstantsApp.CURRENT_USER, user);

        if (!isAjax(httpServletRequest)) {
            // 不是ajax请求
            super.issueSuccessRedirect(request, response);
        } else {
            JsonUtil.writeJson(response, new AjaxResult().success());
        }
        return false;
    }

    @Override
    protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request, ServletResponse response) {
        if (!isAjax((HttpServletRequest) request)) {// 不是ajax请求
            super.setFailureAttribute(request, e);
            return true;
        }
        JsonUtil.writeJson(
                response,
                new AjaxResult<>().fail(401, "登录失败", e.getMessage())
        );
        return false;
    }

    private boolean isAjax(HttpServletRequest httpServletRequest) {
        return "XMLHttpRequest".equalsIgnoreCase(httpServletRequest
                .getHeader("X-Requested-With"));
    }
}