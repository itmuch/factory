package com.itmuch.crawler.security.shiro;

import com.itmuch.crawler.core.constants.ConstantsApp;
import com.itmuch.crawler.core.convert.AjaxResult;
import com.itmuch.crawler.security.jwt.JwtToken;
import com.itmuch.crawler.util.mapper.JsonMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @see ExtendFormAuthenticationFilter
 */
@Slf4j
@Deprecated
public class JwtAuthenticationFilter extends AuthenticatingFilter {

    private String failureUrl;

    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String token = httpRequest.getHeader(ConstantsApp.TOKEN_HEADER);
        return JwtToken.builder()
                .token(token)
                .build();
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
//        Subject subject = getSubject(request, response);
//        if (!subject.isAuthenticated()) {
//            redirectToLogin(request, response);
//            return false;
//        }
        return executeLogin(request, response);
    }

    @Override
    protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request,
                                     ServletResponse response) throws Exception {
        return true;
    }

    @Override
    protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException ae, ServletRequest request,
                                     ServletResponse response) {
        AjaxResult<Object> success = new AjaxResult<>()
                .fail(401, ae.getMessage(), "登录失败，无权访问");
        try {
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json; charset=utf-8");
            response.getWriter()
                    .write(
                            JsonMapper.defaultMapper()
                                    .toJson(success)
                    );
        } catch (IOException e) {
            log.error("发生异常。", e);
        }
        return false;
    }

}
