package com.itmuch.crawler.security.jwt;

import com.itmuch.crawler.config.properties.CrawlerConfigurationProperties;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("ALL")
@Component
public class TokenUtils {
    private final String AUDIENCE_UNKNOWN = "unknown";
    private final String AUDIENCE_WEB = "web";
    private final String AUDIENCE_MOBILE = "mobile";
    private final String AUDIENCE_TABLET = "tablet";

    private final CrawlerConfigurationProperties properties;

    @Autowired
    public TokenUtils(CrawlerConfigurationProperties properties) {
        this.properties = properties;
    }

    private Claims getClaimsFromToken(String token) {
        try {
            return Jwts.parser()
                    .setSigningKey(this.properties.getToken().getSecret())
                    .parseClaimsJws(token)
                    .getBody();
        } catch (ExpiredJwtException e) {
            throw new IllegalArgumentException("token已过期。");
        } catch (UnsupportedJwtException e) {
            throw new IllegalArgumentException("token非法。");
        } catch (MalformedJwtException e) {
            throw new IllegalArgumentException("jwt未能正常被构造。");
        } catch (SignatureException e) {
            throw new IllegalArgumentException("签名计算失败。");
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("token参数非法。");
        }
    }

    public String generateToken(String username) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", username);
        claims.put("audience", "mobile" /*this.generateAudience(device)*/);
        claims.put("created", new Date(System.currentTimeMillis()));
        return this.generateToken(claims);
    }

    private String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + this.properties.getToken().getExpiration() * 1000))
                .signWith(SignatureAlgorithm.HS512, this.properties.getToken().getSecret())
                .compact();
    }


    public String getUsernameFromToken(String token) {
        return this.getClaimsFromToken(token).getSubject();
    }

    public String refreshToken(String token) {
        final Claims claims = this.getClaimsFromToken(token);
        claims.put("created", new Date(System.currentTimeMillis()));
        return this.generateToken(claims);
    }
}
