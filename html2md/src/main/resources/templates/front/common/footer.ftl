<footer class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <span>Copyright &copy; 2017
                </span> |
                <span>
                    Powered by <a href="${ctx}/" class="copyright-links" target="_blank" rel="nofollow">V2LP</a>
                </span>
            </div>
        </div>
    </div>
</footer>