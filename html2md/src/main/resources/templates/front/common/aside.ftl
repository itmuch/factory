<aside class="col-md-4 sidebar">
    <div class="widget notification">
        <h3 class="title">公告</h3>
        <div>
            <p>
                <b>笔者微信</b>：jumping_me，欢迎加我<br/>
                <b>公众号</b>：请扫描我的头像<br/>
                <b>本站主题</b>：<a href="http://github.com/itmuch/hexo-theme-itmuch">下载</a>
            </p>
        </div>
    </div>

    <div class="widget">
        <h3 class="title">友情链接</h3>
        <div class="content friends-link">

            <a href="https://www.feel88.cn/" class="fa" target="_blank">大数据</a>

            <a href="http://blog.didispace.com/" class="fa" target="_blank">程序猿DD|翟永超</a>

        </div>
    </div>
</aside>