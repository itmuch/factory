<#include "../common/layout.ftl">
<!DOCTYPE html>
<html>
<@head title="站点管理 - 管理控制台"></@head>
<@body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            站点
            <small>管理</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${ctx}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">站点</a></li>
            <li class="active">站点管理</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">站点信息</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <table id="table"></table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

</@body>
<script>
    var $table = $('#table');
    // 初始化表格
    $table.bootstrapTable({
        search: true,
        sidePagination: 'server',
        pagination: 'true',
        url: '${ctx}/admin/sites',
        queryParams: function (params) {
            // 一页展示几条
            var limit = params.limit;
            // 第几页
            var page = params.offset / limit + 1;
            // 搜索词
            var search = params.search;

            var par = {};
            par.page = page;
            par.rows = limit;
            if (search !== null && search !== '') {
                par.name = search;
            }
            return par;
        },
        columns: [{
            title: 'ID',
            field: 'id'
        }, {
            field: 'name',
            title: '名称'

        },{
            field: 'type',
            title:'类型'
        },{
            field: 'url',
            title: '首页'

        },{
            field: 'createTime',
            title: '创建时间'

        },{
            field: 'updateTime',
            title: '修改时间'

        }],
        responseHandler: function (res) {
            res.rows = res.content;
            res.total = res.totalElements;
            return res;
        }
    });


</script>
</html>