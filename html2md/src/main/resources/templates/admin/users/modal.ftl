<div class="modal fade" id="edit-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">修改用户</h4>
            </div>

            <div class="modal-body">
                <form role="form" id="edit-form">
                    <div class="form-group">
                        <label for="username">用户名</label>
                        <input type="hidden" name="id" id="edit-id">
                        <input name="username" id="edit-username" type="text" class="form-control"
                               placeholder="请输入站点名称">
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" id="edit-save">保存</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->