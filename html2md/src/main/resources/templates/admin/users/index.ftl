<#include "../common/layout.ftl">
<!DOCTYPE html>
<html>
<@head title="用户管理 - 管理控制台"></@head>
<@body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            用户
            <small>管理</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${ctx}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">用户</a></li>
            <li class="active">用户管理</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">用户信息</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="toolbar">
                            <div class="box-header with-border">
                            <#--<button class="btn btn-primary" onclick="add()">-->
                            <#--<i class="fa fa-save"></i> 添加-->
                            <#--</button>-->
                                <button class="btn btn-success" onclick="edit()">
                                    <i class="fa fa-edit"></i> 修改
                                </button>
                            <#--<button class="btn btn-danger" onclick="batchRemove();">-->
                            <#--<i class="fa fa-remove"></i> 批量删除-->
                            <#--</button>-->
                            </div>
                        </div>
                        <table id="table"></table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

    <#include "modal.ftl">

</@body>
<script>
    var $table = $('#table');
    // 初始化表格
    $table.bootstrapTable({
        search: true,
        sidePagination: 'server',
        pagination: 'true',
        url: '${ctx}/admin/users',
        toolbar: '#toolbar',
        queryParams: function (params) {
//            alert(JSON.stringify(params));
            // 一页展示几条
            var limit = params.limit;
            // 第几页
            var page = params.offset / limit + 1;

            // 搜索词
            var search = params.search;

            var par = {};
            par.page = page;
            par.rows = limit;

            if (search !== null && search !== '') {
                par.username = search;
            }
            return par;
        },
        columns: [{
            field: 'state',
            checkbox: true
        }, {
            title: 'ID',
            field: 'id'
        }, {
            field: 'username',
            title: '用户名'
        }],
        responseHandler: function (res) {
            res.rows = res.content;
            res.total = res.totalElements;
            return res;
        }
    });

    // 点击修改按钮
    function edit() {
        var selected = getSelections();
        var selectId = getIdSelections();
        if (selectId === null) {
            swal('错误', '请勾选要修改的用户', "error");
            return;
        }
        if (selectId.length !== 1) {
            swal('错误', '请勾选, 并确保仅勾选一条用户信息', "error");
            return;
        }
        // 允许编辑准备数据
        $('#edit-id').val(selectId);
        $('#edit-username').val(selected[0].username);

        // 弹出模态框
        $("#edit-modal").modal();
    }

    $('#edit-save').click(function () {
        var formJson = $('#edit-form').serializeJSON();
        $.ajax({
            url: '${ctx}/admin/users',
            dataType: "json",
            data: JSON.stringify(formJson),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                swal('操作成功', JSON.stringify(data), "success");
            },
            error: function (data) {
                swal('错误', JSON.stringify(data), "error");
            }
        });
    });

    // 获取表格选中项（数组）
    function getSelections() {
        return $table.bootstrapTable('getSelections');
    }

    // 获得表格选中项的id数组
    function getIdSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.id;
        });
    }
</script>
</html>