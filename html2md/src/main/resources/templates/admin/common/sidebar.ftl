<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">导航栏</li>
            <li class="treeview menu-open active">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>导航</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu ">
                    <li><a href="${ctx}/admin/users/index"><i class="fa fa-circle-o"></i> 用户管理</a></li>
                    <li><a href="${ctx}/admin/jobs/index"><i class="fa fa-circle-o"></i> 任务管理</a></li>
                    <li><a href="${ctx}/admin/sites/index"><i class="fa fa-circle-o"></i> 站点管理</a></li>
                    <li><a href="${ctx}/admin/articles/index"><i class="fa fa-circle-o"></i> 文章管理</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>