<!DOCTYPE HTML>
<html>
<head>
<#include "../../../common/meta.ftl">
    <title>任务管理 - 添加</title>

<#include "../../../common/link.ftl">
</head>
<#include "../../../common/ie9.ftl">
<body>
<#include "../../../common/header.ftl">

<section class="content-wrap">
    <div class="container">
        <div class="row">
            <main class="col-md-12 main-content">
                <article class="post col-md-6" style="border-right:1px solid black;">
                    <div class="post-head">
                        <h1>Job配置</h1>
                    </div>

                    <div class="post-body">
                        <form role="form" id="mainForm">
                            <div class="form-group">
                                <label for="site-name">站点名称(*)</label>
                                <input name="site[name]" id="site-name" type="text" class="form-control"
                                       placeholder="请输入站点名称">
                                <p class="help-block">请输入站点名称。</p>
                            </div>

                            <div class="form-group">
                                <label for="site-url">站点URL(*)</label>
                                <input name="site[url]" id="site-url" type="text" class="form-control"
                                       placeholder="请输入站点名称">
                                <p class="help-block">请输入站点URL。</p>
                            </div>

                            <div class="form-group">
                                <label for="site-type">站点类型(*)</label>
                                <select class="form-control" name="site[type]" id="site-type">
                                    <option value="1" selected>1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="name">Job名称(*)</label>
                                <input name="name" id="name" type="text" class="form-control"
                                       placeholder="请输入Job名称">
                                <p class="help-block">请输入Job名称。</p>
                            </div>

                            <div class="form-group">
                                <label for="group">JobGroup名称</label>
                                <input name="group" id="group" type="text" class="form-control"
                                       placeholder="请输入Group名称">
                                <p class="help-block">留空则默认为JOB_DEFAULT_GROUP。</p>
                            </div>

                            <div class="form-group">
                                <label for="cronExpression">Cron表达式</label>
                                <input name="cronExpression" id="cronExpression" type="text" class="form-control"
                                       placeholder="请输入Cron表达式">
                                <p class="help-block">请输入Cron表达式。示例：0 * * * * ? 。
                                    <a href="http://blog.csdn.net/eacter/article/details/44308459" target="_blank">Cron参考文档</a>
                                </p>

                            </div>

                            <div class="form-group">
                                <label for="simpleExpression">简单时间表达式</label>
                                <input name="simpleExpression" id="simpleExpression" type="text" class="form-control"
                                       placeholder="请输入简单时间表达式">
                                <p class="help-block">如同时也填写了Cron表达式，则以Cron表达式为准。</p>
                            </div>

                            <div class="form-group">
                                <label for="jobClassName">Job的类名(*)</label>
                                <input name="jobClassName" id="jobClassName" type="text" class="form-control"
                                       placeholder="请输入Job的类名">
                                <p class="help-block">示例：com.itmuch.crawler.quartz.job.BlogCrawlerJob</p>
                            </div>

                            <div class="form-group">
                                <label for="jobMethodName">Job的方法名(*)</label>
                                <input name="jobMethodName" id="jobMethodName" type="text" class="form-control"
                                       placeholder="请输入Job的方法名">
                                <p class="help-block">请输入Job的方法名</p>
                            </div>

                            <div class="form-group">
                                <label for="jobParam">Job的参数</label>
                                <textarea name="jobParam" id="jobParam" class="form-control"
                                          placeholder="请输入Job的参数" rows="13"></textarea>
                                <p class="help-block">参数请使用Json格式。对于复杂对象，可使用页面右侧的工具生成模板。</p>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="concurrent" name="concurrent" checked>是否并发，默认是
                                </label>
                            </div>

                            <div class="form-group">
                                <label for="description">Job描述</label>
                                <textarea name="description" id="description" class="form-control"
                                          placeholder="请输入Job的描述"></textarea>
                                <p class="help-block">请输入Job的描述</p>

                            </div>

                            <button type="button" id="submit-btn" class="btn btn-default">提交</button>
                        </form>
                    </div>
                </article>


                <article class="post col-md-6">
                    <div class="post-head">
                        <h1>Job参数模板</h1>
                    </div>
                    <div class="post-body">

                        <div class="form-group">
                            <label for="jobParamType">Job的参数的类名</label>
                            <input name="jobParamType" id="jobParamType" type="text" class="form-control"
                                   placeholder="请输入Job的参数的类名">
                            <p class="help-block">
                                请输入Job的参数的类名。示例：<br>
                                com.itmuch.crawler.webmagic.config.BlogCrawlerConfiguration</p>
                        </div>
                        <pre id="jobParamTemplate" style="border: solid 1px #ddd"></pre>
                    </div>
                </article>
            </main>
        </div>
    </div>
</section>
<#include "../../../common/footer.ftl">
<script src="http://www.itmuch.com/lib/jquery/index.js?rev=2.1.3"></script>
<script type="text/javascript" src="${ctx}/webjars/jquery.serializeJSON/2.8.1/jquery.serializejson.min.js"></script>
<script>
    // 模板生成工具
    $('#jobParamType').blur(function () {
        var clazz = $('#jobParamType').val();
        $.ajax({
            url: '${ctx}/admin/jobs/gen-param-template?clazz=' + clazz,
            dataType: "json",
            type: "GET",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                // 格式化输出json到页面。
                var result = JSON.stringify(data, null, 4);
                $('#jobParamTemplate').html(result);
            },
            error: function () {
                alert(JSON.stringify(data, null, 4));
            }
        });
    });


    $('#submit-btn').click(function () {
        var formJson = $('#mainForm').serializeJSON();
        var concurrent = false;
        if ($('#concurrent').attr("checked")) {
            concurrent = true;
        }
        formJson.concurrent = concurrent;
        $.ajax({
            url: '${ctx}/admin/jobs',
            dataType: "json",
            data: JSON.stringify(formJson),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                alert(JSON.stringify(data, null, 4));
            },
            error: function (data) {
                alert(JSON.stringify(data, null, 4));
            }
        });
    });
</script>
</body>


</html>