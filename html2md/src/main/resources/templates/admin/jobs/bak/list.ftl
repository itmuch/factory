<!DOCTYPE HTML>
<html>
<head>
<#include "../../../common/meta.ftl">
    <title>任务管理 - 列表</title>

<#include "../../../common/link.ftl">
</head>
<#include "../../../common/ie9.ftl">
<body>
<#include "../../../common/header.ftl">

<section class="content-wrap">
    <div class="container">
        <div class="row">
            <main class="col-md-12 main-content">
                <article class="post col-md-12">
                    <a class="btn btn-primary" href="${ctx}/admin/jobs/add" target="_blank">添加</a>

                    <table class="table">
                        <thead>
                        <tr>
                            <th>名称</th>
                            <th>Group</th>
                            <th>并发</th>
                            <th>创建时间</th>
                            <th>修改时间</th>
                            <th>Cron表达式</th>
                            <th>Simple表达式</th>
                            <th>状态</th>
                        <#--<th>Job类名</th>-->
                        <#--<th>Job方法名</th>-->
                        <#--<th>Job参数</th>-->
                        <#--<th>Job描述</th>-->
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <#assign jobs=page.content>
                        <#list jobs as job>
                        <tr>
                            <td>${(job.name)!}</td>
                            <td>${(job.group)!}</td>
                            <th>${(job.concurrent)?c}</th>
                            <td>${(job.createTime?string('yyyy-MM-dd'))!'无数据'}</td>
                            <td>${(job.updateTime?string('yyyy-MM-dd'))!'无数据'}</td>
                            <td>${(job.cronExpression)!}</td>
                            <td>${(job.simpleExpression)!}</td>
                            <td>${(job.status)!}</td>
                        <#--<td>${(job.jobClassName)!}</td>-->
                        <#--<td>${(job.jobMethodName)!}</td>-->
                        <#--<td>-->
                        <#--<pre>${(job.jobParam)!}</pre>-->
                        <#--</td>-->
                        <#--<td>${(job.description)!}</td>-->
                            <td>
                                <a href="${ctx}/admin/jobs/edit/${job.id}" target="_blank">修改</a>

                                <a class="pause" href="javascript:void(0)" target="_blank">
                                    <input type="hidden" value="${ctx}/admin/jobs/${job.id}/pause">
                                    暂停
                                </a>

                                <a class="resume" href="javascript:void(0)" target="_blank">
                                    <input type="hidden" value="${ctx}/admin/jobs/${job.id}/resume">
                                    恢复
                                </a>


                                <#--<a class="del" href="javascript:void(0)" target="_blank">-->
                                    <#--<input type="hidden" value="${ctx}/admin/jobs/${job.id}">-->
                                    <#--删除-->
                                <#--</a>-->
                            </td>
                        </tr>
                        </#list>
                        </tbody>
                    </table>


                </article>
            </main>
        </div>
    </div>
</section>
<#include "../../../common/footer.ftl">
<script src="http://www.itmuch.com/lib/jquery/index.js?rev=2.1.3"></script>
<script>
    // 暂停Job
    $('.pause').click(function () {
        var href = $(this).find('input').val();

        $.ajax({
            url: href,
            dataType: "json",
            type: "PATCH",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                window.location.href = '${ctx}/admin/jobs/list'

            },
            error: function (data) {
                alert(JSON.stringify(data, null, 4));
            }
        });
    });

    // 恢复
    $('.resume').click(function () {
        var href = $(this).find('input').val();

        $.ajax({
            url: href,
            dataType: "json",
            type: "PATCH",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                window.location.href = '${ctx}/admin/jobs/list'

            },
            error: function (data) {
                alert(JSON.stringify(data, null, 4));
            }
        });
    });


    $('.del').click(function () {
        var href = $(this).find('input').val();

        $.ajax({
            url: href,
            dataType: "json",
            type: "DELETE",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                window.location.href = '${ctx}/admin/jobs/list'

            },
            error: function (data) {
                alert(JSON.stringify(data, null, 4));
            }
        });
    });




</script>
</body>
</html>

