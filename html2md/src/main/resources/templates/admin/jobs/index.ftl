<#include "../common/layout.ftl">
<!DOCTYPE html>
<html>
<@head title="任务管理 - 管理控制台"></@head>
<@body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            任务
            <small>管理</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${ctx}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">任务</a></li>
            <li class="active">任务管理</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">任务信息</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="toolbar">
                            <div class="box-header with-border">
                                <button class="btn btn-primary" onclick="add()">
                                    <i class="fa fa-save"></i> 添加
                                </button>
                                <button class="btn btn-success" onclick="edit()">
                                    <i class="fa fa-edit"></i> 修改
                                </button>
                                <button class="btn btn-danger" onclick="pause()">
                                    <i class="fa fa-edit"></i> 暂停
                                </button>
                                <button class="btn btn-default" onclick="resume()">
                                    <i class="fa fa-edit"></i> 恢复
                                </button>
                            </div>
                        </div>
                        <table id="table"></table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

    <#include "modal.ftl">

</@body>
<script>
    var $table = $('#table');
    // 初始化表格
    $table.bootstrapTable({
        search: true,
        showRefresh: true,
        sidePagination: 'server',
        pagination: 'true',
        url: '${ctx}/admin/jobs',
        toolbar: '#toolbar',
        queryParams: function (params) {
            // 一页展示几条
            var limit = params.limit;
            // 第几页
            var page = params.offset / limit + 1;
            // 搜索词
            var search = params.search;

            var par = {};
            par.page = page;
            par.rows = limit;
            if (search !== null && search !== '') {
                par.name = search;
            }
            return par;
        },
        columns: [{
            field: 'state',
            checkbox: true,
        }, {
            title: 'ID',
            field: 'id'
        }, {
            field: 'name',
            title: '任务名称'
        }, {
            field: 'group',
            title: '组名称'
        }, {
            field: 'concurrent',
            title: '并发性'
        }, {
            field: 'createTime',
            title: '创建时间'
        }, {
            field: 'updateTime',
            title: '修改时间'
        }, {
            field: 'cronExpression',
            title: 'Cron表达式'
        }, {
            field: 'simpleExpression',
            title: 'Simple表达式'
        }, {
            field: 'status',
            title: '状态'
        }],
        responseHandler: function (res) {
            res.rows = res.content;
            res.total = res.totalElements;
            return res;
        }
    });

    function add() {
        $('#httpMethod').val('POST');
        $('#main-form')[0].reset();
        $("#main-modal").modal();
    }

    // 模态框中右侧的生成模板按钮
    $('#jobParamType').blur(function () {
        var clazz = $('#jobParamType').val();
        $.ajax({
            url: '${ctx}/admin/jobs/gen-param-template?clazz=' + clazz,
            dataType: "json",
            type: "GET",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                // 格式化输出json到页面。
                var result = JSON.stringify(data, null, 4);
                $('#jobParamTemplate').html(result);
            },
            error: function (data) {
                swal('错误', JSON.stringify(data), "error");
            }
        });
    });

    // 点击保存按钮
    $('#save').click(function () {
        var formJson = $('#main-form').serializeJSON();
        var concurrent = false;
        if ($('#concurrent').attr("checked")) {
            concurrent = true;
        }
        formJson.concurrent = concurrent;
        var method = $('#httpMethod').val();
        $.ajax({
            url: '${ctx}/admin/jobs',
            dataType: "json",
            data: JSON.stringify(formJson),
            type: method,
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                swal('操作成功', JSON.stringify(data), "success");
            },
            error: function (data) {
                swal('错误', JSON.stringify(data), "error");
            }
        });
    });

    // 点击修改按钮
    function edit() {
        var selects = getSelections();
        var select = selects[0];
        var id = getIdSelections();
        if (id === null) {
            swal('错误', '请勾选要修改的任务', "error");
            return;
        }
        if (id.length !== 1) {
            swal('错误', '请勾选, 并确保仅勾选一条任务信息', "error");
            return;
        }
        // 允许编辑准备数据
        $('#id').val(id);
        $('#name').val(select.name);
        $('#site-name').val(select.site.name);
        $('#site-url').val(select.site.url);
        $('#group').val(select.group);
        $('#cronExpression').val(select.cronExpression);
        $('#simpleExpression').val(select.simpleExpression);
        $('#jobClassName').val(select.jobClassName);
        $('#jobMethodName').val(select.jobMethodName);
        $('#jobParam').val(select.jobParam);
        $('#concurrent').val(select.concurrent);
        $('#description').val(select.description);

        $('#httpMethod').val('PUT');
        // 弹出模态框
        $("#main-modal").modal();
    }


    // 点击暂停按钮
    function pause() {
        var selects = getSelections();
        var select = selects[0];
        var id = getIdSelections();
        if (id === null) {
            swal('错误', '请勾选要暂停的任务', "error");
            return;
        }
        if (id.length !== 1) {
            swal('错误', '请勾选, 并确保仅勾选一条任务信息', "error");
            return;
        }
        $.ajax({
            url: '${ctx}/admin/jobs/' + id + '/pause',
            dataType: "json",
            type: 'PATCH',
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                swal('操作成功', JSON.stringify(data) + '稍后请刷新表格', "success");
            },
            error: function (data) {
                swal('错误', JSON.stringify(data), "error");
            }
        });
    }

    // 点击恢复按钮
    function resume() {
        var selects = getSelections();
        var select = selects[0];
        var id = getIdSelections();
        if (id === null) {
            swal('错误', '请勾选要恢复的任务', "error");
            return;
        }
        if (id.length !== 1) {
            swal('错误', '请勾选, 并确保仅勾选一条任务信息', "error");
            return;
        }
        $.ajax({
            url: '${ctx}/admin/jobs/' + id + '/resume',
            dataType: "json",
            type: 'PATCH',
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                swal('操作成功', JSON.stringify(data) + '稍后请刷新表格', "success");
            },
            error: function (data) {
                swal('错误', JSON.stringify(data), "error");
            }
        });
    }


    // 获取表格选中项（数组）
    function getSelections() {
        return $table.bootstrapTable('getSelections');
    }

    // 获得表格选中项的id数组
    function getIdSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.id;
        });
    }
</script>
</html>