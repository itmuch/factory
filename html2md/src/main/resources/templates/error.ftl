<html>
<body><h1>Whitelabel Error Page</h1>
<p>This application has no explicit mapping for /error, so you are seeing this as a fallback.</p>
<div id='created'>${timestamp?date}</div>
<div>There was an unexpected error (type=${error!}, status=${status!}).</div>
<div>${message!}</div>
</body>
</html>