package com.itmuch.mybatisgen.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MybatisGenConfiguration {
    /**
     * 项目的根路径
     */
    private String projectPath;

    /**
     * 实体类包路径
     */
    private String entityPackageName;

    /**
     * DAO Java类包名
     */
    private String daoPackageName;

    /**
     * DAO XML文件包名
     */
    private String daoXMLPackageName;

    /**
     * 实体类名
     */
    private String entityName;

    /**
     * DAO类名
     */
    private String daoName;
}
