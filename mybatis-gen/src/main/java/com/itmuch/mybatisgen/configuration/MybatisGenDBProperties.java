package com.itmuch.mybatisgen.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "mybatis.gen.datasource")
@Data
@Configuration
@EnableConfigurationProperties(MybatisGenDBProperties.class)
public class MybatisGenDBProperties {

    private String url;
    private String username;
    private String password;
    private String driverClassName;
}
