package com.itmuch.mybatisgen.controller;

import com.itmuch.mybatisgen.configuration.MybatisGenConfiguration;
import com.itmuch.mybatisgen.service.GenService;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@Controller
@Slf4j
public class GenController {
    @Autowired
    private GenService genService;

    @GetMapping("")
    private String index() {
        return "index";
    }

    @PostMapping("/gen")
//    @ResponseBody
    public ResponseEntity<byte[]> test(
            @ModelAttribute MybatisGenConfiguration configuration,
            HttpServletResponse response
    ) throws SQLException, ClassNotFoundException, IOException, TemplateException {
        String result = genService.gen("f_test", configuration);
        log.info("生成文件的字符串：{}", result);

        HttpHeaders headers = new HttpHeaders();
        //解决中文名称乱码问题
        String fileName=new String("生成的文件.txt".getBytes("UTF-8"),"iso-8859-1");
        headers.setContentDispositionFormData("attachment", fileName);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<byte[]>(result.getBytes(),
                headers, HttpStatus.CREATED);

    }
}
