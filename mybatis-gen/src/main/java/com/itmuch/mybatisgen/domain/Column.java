package com.itmuch.mybatisgen.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Column {
    /**
     * 字段名称
     */
    protected String name;

    /**
     * jdbc的类型
     */
    protected String jdbcType;

    /**
     * jdbc类型所对应的Java类型
     */
    private String javaType;

    /**
     * 实体类的field
     */
    protected String property;

    /**
     * 注释
     */
    protected String remark;

    /**
     * 是否主键
     */
    private boolean isPrimaryKey;
}
