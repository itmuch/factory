package com.itmuch.mybatisgen.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Table {
    private List<Column> idColumns;
    private List<Column> columns;
}
