package com.itmuch.mybatisgen.util;

import com.itmuch.mybatisgen.configuration.MybatisGenDBProperties;
import com.itmuch.mybatisgen.domain.Column;
import com.itmuch.mybatisgen.domain.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DataSourceUtil {
    // 字段名称
    public static final String COLUMN_NAME = "COLUMN_NAME";
    // 字段的数据库类型
    public static final String TYPE_NAME = "TYPE_NAME";
    // 字段注释
    public static final String REMARKS = "REMARKS";
    // 字段长度大小
    public static final String COLUMN_SIZE = "COLUMN_SIZE";

    @Qualifier("dataSource")
    @Autowired

    private DataSource dataSource;
    @Autowired
    private MybatisGenDBProperties mybatisGenDBProperties;

    private Connection connection;

    public void getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(mybatisGenDBProperties.getDriverClassName());

        this.connection = DriverManager.getConnection(
                mybatisGenDBProperties.getUrl(),
                mybatisGenDBProperties.getUsername(),
                mybatisGenDBProperties.getPassword()
        );
    }

    public List<String> getIDColumnsByTableName(String tableName) throws SQLException, ClassNotFoundException {
        List<String> idColumns = new LinkedList<>();
        DatabaseMetaData metaData = getDatabaseMetaData();
        ResultSet rs = metaData.getPrimaryKeys(this.connection.getCatalog(), "", tableName);

        while (rs.next()) {
            String columnName = rs.getString("COLUMN_NAME");
            idColumns.add(columnName);
        }
        return idColumns;
    }


    public List<Column> getColumnsByTableName(String tableName) throws SQLException, ClassNotFoundException {
        List<Column> columns = new LinkedList<>();
        DatabaseMetaData metaData = getDatabaseMetaData();
        ResultSet rs = metaData.getColumns(null, metaData.getUserName(), tableName, "%");

        while (rs.next()) {

            String columnName = rs.getString(COLUMN_NAME);
            String jdbcType = rs.getString(TYPE_NAME);
            String remarks = rs.getString(REMARKS);
            int columnSize = rs.getInt(COLUMN_SIZE);

            String javaType = JavaTypeConverter.javaType(jdbcType, columnSize);

            columns.add(
                    Column.builder()
                            .jdbcType(jdbcType)
                            .javaType(javaType)
                            .name(columnName)
                            .remark(remarks)
                            .property(Underline2Camel.underline2Camel(columnName, true))
                            .build()
            );

        }
        // todo RELEASE connection
        return columns;
    }


    public Table generatorTableObject(String tableName) throws SQLException, ClassNotFoundException {
        List<String> idNames = this.getIDColumnsByTableName(tableName);
        List<Column> columnsContainId = this.getColumnsByTableName(tableName);

        Map<String, Column> map = columnsContainId
                .stream()
                .collect(
                        Collectors.toMap(Column::getName, t -> t)
                );

        Set<String> keys = map.keySet();
        List<Column> idColumns = keys
                .stream()
                .filter(idNames::contains)
                .map(map::get)
                .collect(Collectors.toList())
                .stream()
                .peek(t -> t.setPrimaryKey(true))
                .collect(Collectors.toList());

        List<Column> columns = keys
                .stream()
                .filter(t -> !idNames.contains(t))
                .map(map::get)
                .collect(Collectors.toList());

        return Table.builder()
                .idColumns(idColumns)
                .columns(columns)
                .build();

    }

    private DatabaseMetaData getDatabaseMetaData() throws ClassNotFoundException, SQLException {
        // TODO: 2017/10/23 这里设计有点奇怪，待优化
        this.getConnection();
        return this.connection.getMetaData();
    }
}
