package com.itmuch.mybatisgen.util;

public class JavaTypeConverter {

    public static final String SHORT = "java.lang.Short";
    public static final String INTEGER = "java.lang.Integer";
    public static final String LONG = "java.lang.Long";
    public static final String BIGDECIMAL = "java.math.BigDecimal";

    // ref: http://blog.csdn.net/isea533/article/details/42102297
    public static String javaType(String jdbcType, int size) {
        String javaType = null;
        switch (jdbcType) {
            case "INT":
                if (size < 5) {
                    javaType =  SHORT;
                }
                else if (size <= 9) {
                    javaType =  INTEGER;
                }
                else if (size <= 18) {
                    javaType = LONG;
                }
                else if (size > 18) {
                    javaType =  BIGDECIMAL;
                }
                break;
            case "DECIMAL":
                javaType =  BIGDECIMAL;
                break;
            case "VARCHAR":
                javaType =  "java.lang.String";
                break;
            case "BIGINTEGER":
                javaType = LONG;
        };
        return javaType;
    }
}
