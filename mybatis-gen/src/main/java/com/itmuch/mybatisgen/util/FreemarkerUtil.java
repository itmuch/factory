package com.itmuch.mybatisgen.util;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;

public class FreemarkerUtil {

    public static void generateFile(String fileName, String templateName, Map<Object, Object> map){
        Configuration config = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        config.setDefaultEncoding("UTF-8");
        config.setTemplateLoader(new ClassTemplateLoader(FreemarkerUtil.class, "/"));
        config.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        try{
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
            Template template = config.getTemplate(templateName);
            template.process(map, out);
            out.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static String generateString(String templateName, Map<Object, Object> map) throws IOException, TemplateException {
        Configuration config = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        config.setDefaultEncoding("UTF-8");
        config.setTemplateLoader(new ClassTemplateLoader(FreemarkerUtil.class, "/"));
        config.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        Template template = config.getTemplate(templateName);

        return FreeMarkerTemplateUtils.processTemplateIntoString(template, map);
    }

}
