package com.itmuch.mybatisgen.service;

import com.google.common.collect.Maps;
import com.itmuch.mybatisgen.configuration.MybatisGenConfiguration;
import com.itmuch.mybatisgen.domain.Table;
import com.itmuch.mybatisgen.util.DataSourceUtil;
import com.itmuch.mybatisgen.util.FreemarkerUtil;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

@Service
public class GenService {
    @Autowired
    private DataSourceUtil dataSourceUtil;

    public Table getTabelObject(String tableName) throws SQLException, ClassNotFoundException {
        return this.dataSourceUtil.generatorTableObject(tableName);
    }

    public String gen(String tableName, MybatisGenConfiguration configuration) throws SQLException, ClassNotFoundException, IOException, TemplateException {
        Table table = this.getTabelObject(tableName);

        String projectPath = configuration.getProjectPath();
        String daoPackageName = configuration.getDaoPackageName();
        String daoName = configuration.getDaoName();
        String entityPackageName = configuration.getEntityPackageName();
        String entityName = configuration.getEntityName();
        String daoXMLPackageName = configuration.getDaoXMLPackageName();

        HashMap<Object, Object> map = Maps.newHashMap();
        map.put("table", table);
        map.put("projectPath", projectPath);
        map.put("daoPackageName", daoPackageName);
        map.put("daoName", daoName);
        map.put("entityPackageName", entityPackageName);
        map.put("entityName", entityName);

        String xmlPath = String.format(
                "%s/src/main/resources/%s/%s.xml",
                projectPath,
                daoXMLPackageName,
                daoName
        );

        // mapper XML 字符串
        String mapperXML = FreemarkerUtil.generateString("mybatis-gen-templates/MapperXML.ftl", map);
        String mapperJava = FreemarkerUtil.generateString("mybatis-gen-templates/MapperJava.ftl", map);
        String entity = FreemarkerUtil.generateString("mybatis-gen-templates/Entity.ftl", map);

        return String.format("%s\n%s\n%s", mapperXML, mapperJava, entity);
    }
}
