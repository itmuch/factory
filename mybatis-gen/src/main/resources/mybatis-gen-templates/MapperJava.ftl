package ${daoPackageName};

import com.github.abel533.mapper.Mapper;
import ${entityPackageName}.${entityName};

public interface ${daoName} extends Mapper<${entityName}> {
}