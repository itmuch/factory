

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
public class ${entityName} {

<#list table.idColumns as idColumn>
    /**
     * ${idColumn.remark}
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private ${idColumn.javaType} ${idColumn.property};
</#list>
<#list table.columns as column>
    /**
     * ${column.remark}
     */
    @Column(name = "${column.name}")
    private ${column.javaType} ${column.property};
</#list>
}