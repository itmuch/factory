<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${daoPackageName}.${daoName}">
    <resultMap id="BaseResultMap" type="${entityPackageName}.${entityName}">
    <#list table.idColumns as idColumn>
        <id property="${idColumn.property}" column="${idColumn.name}" jdbcType="${idColumn.jdbcType}"/>
    </#list>
    <#list table.columns as column>
        <result property="${column.property}" column="${column.name}" jdbcType="${column.jdbcType}"/>
    </#list>
    </resultMap>
</mapper>