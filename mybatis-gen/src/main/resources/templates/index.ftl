<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Mybatis 代码生成器配置页</title>
    <style>
        input {
            width: 400px;
        }
    </style>
</head>
<body>

<form <#--id="mainForm"--> method="post" action="/gen">
    项目路径：<input type="text" id="projectPath" name="projectPath" value="/Users/apple/Desktop/mybatis-gen"><br>
    DAO类的包名：<input type="text" id="daoPackageName" name="daoPackageName" value="com.itmuch.mapper"><br>
    DAO XML的包名：<input type="text" id="daoXMLPackageName" name="daoXMLPackageName" value="com.itmuch.xml"><br>
    DAO类的类名：<input type="text" id="daoName" name="daoName" value="testDAO"><br>
    实体类的包名：<input type="text" id="entityPackageName" name="entityPackageName" value="com.itmuch.entity"><br>
    实体的类名：<input type="text" id="entityName" name="entityName" value="TestPO"><br>
    <input type="submit" value="提交">
    <#--<button type="submit">提交</button>-->
</form>

<pre id="result"></pre>

</body>
<script type="text/javascript" src="/webjars/jquery/3.2.1/jquery.js"></script>
<#--<script>
    var projectPath = $('#projectPath').val();
    var daoPackageName = $('#daoPackageName').val();
    var daoXMLPackageName = $('#daoXMLPackageName').val();
    var daoName = $('#daoName').val();
    var entityPackageName = $('#entityPackageName').val();
    var entityName = $('#entityName').val();

    $.ajax({
        url: "/gen",
        dataType: "json",
        data: JSON.stringify({
            "projectPath": projectPath,
            "daoPackageName": daoPackageName,
            "daoXMLPackageName": daoXMLPackageName,
            "daoName": daoName,
            "entityPackageName": entityPackageName,
            "entityName": entityName
        }),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            $('#result').html(JSON.stringify(data));
        },
        error: function (data) {
            $('#result').html(JSON.stringify(data));
        }
    });
</script>-->
</html>